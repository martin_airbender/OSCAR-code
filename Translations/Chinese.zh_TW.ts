<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="88"/>
        <source>Sorry, could not locate About file.</source>
        <translation>歹勢，找不到相關檔案。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="238"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>&amp;關於</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>GPL授權</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="163"/>
        <source>Build Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="40"/>
        <source>About OSCAR %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="103"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>歹勢，找不到此志工名單。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="130"/>
        <source>OSCAR %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="133"/>
        <source>Important:</source>
        <translation>重要提示:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>志工名單</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="118"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>歹勢，找不到版本說明。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="134"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="147"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>授權說明%1。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Release Notes</source>
        <translation>版本說明</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="36"/>
        <source>Show data folder</source>
        <translation>顯示數據資料夾</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="880"/>
        <source>Could not find the oximeter file:</source>
        <translation>歹勢，找不到血氧儀檔案:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="886"/>
        <source>Could not open the oximeter file:</source>
        <translation>歹勢，無法開啟血氧儀檔案:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="480"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>無法傳輸血氧儀資料。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="480"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>請確認已在血氧儀選單中選取&apos;上传&apos;操作.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="548"/>
        <source>Could not find the oximeter file:</source>
        <translation>歹勢，找不到血氧儀檔案:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="554"/>
        <source>Could not open the oximeter file:</source>
        <translation>歹勢，無法開啟血氧儀檔案:</translation>
    </message>
</context>
<context>
    <name>CheckUpdates</name>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="250"/>
        <source>Checking for newer OSCAR versions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="1062"/>
        <source>B</source>
        <translation>粗</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1074"/>
        <source>u</source>
        <translation>線</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> 斜 </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Big</source>
        <translation>大</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1467"/>
        <source>End</source>
        <translation>結束</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1323"/>
        <source>99.5%</source>
        <translation>99.5%</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1026"/>
        <source>Oximetry Sessions</source>
        <translation>血氧飽和度監測時段</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1084"/>
        <source>Color</source>
        <translation>顏色</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1492"/>
        <source>Flags</source>
        <translation>記號</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1391"/>
        <source>Notes</source>
        <translation>附註</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1106"/>
        <location filename="../oscar/daily.ui" line="1116"/>
        <source>Small</source>
        <translation>小</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1467"/>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1276"/>
        <source>PAP Mode: %1</source>
        <translation>PAP 模式: %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1196"/>
        <source>I&apos;m feeling ...</source>
        <translation>我目前感覺...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>日記</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1406"/>
        <source>Total time in apnea</source>
        <translation>睡眠呼吸中止總計時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1032"/>
        <source>Position Sensor Sessions</source>
        <translation>位置感測器監控時段</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1363"/>
        <source>Add Bookmark</source>
        <translation>加入書籤</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1399"/>
        <source>Remove Bookmark</source>
        <translation>移除書籤</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2100"/>
        <source>Pick a Colour</source>
        <translation>挑一顏色</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1783"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>請找出產品序號，即刻聯繫您的設備供應商!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1003"/>
        <source>Session Information</source>
        <translation>療程資訊</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1772"/>
        <source>Sessions all off!</source>
        <translation>所有療程結束!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="752"/>
        <source>%1 event</source>
        <translation>%1 重點事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>移至最近一天的數據資料</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1111"/>
        <source>Machine Settings</source>
        <translation>機器處方設定值</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1782"/>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation>歹勢，此機器僅提供醫囑數據資料。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1326"/>
        <source>B.M.I.</source>
        <translation>身體質量指數</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1029"/>
        <source>Sleep Stage Sessions</source>
        <translation>睡眠狀態療程</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1244"/>
        <source>Oximeter Information</source>
        <translation>血氧飽和濃度器資訊</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>重點事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1544"/>
        <source>Graphs</source>
        <translation>圖表</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1023"/>
        <source>CPAP Sessions</source>
        <translation>PAP 療程</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1121"/>
        <source>Medium</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1386"/>
        <source>Starts</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1212"/>
        <source>Weight</source>
        <translation>體重</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1184"/>
        <source>Zombie</source>
        <translation>遲鈍</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1219"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1342"/>
        <source>Bookmarks</source>
        <translation>書籤集</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="797"/>
        <source>Session End Times</source>
        <translation>療程結束次數</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1066"/>
        <source>enable</source>
        <translation>啟用</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="753"/>
        <source>%1 events</source>
        <translation>%1 重點事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>events</source>
        <translation>重點事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1781"/>
        <source>BRICK :(</source>
        <translation>崩潰 Orz</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1491"/>
        <source>Event Breakdown</source>
        <translation>重點事件解析</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="300"/>
        <source>UF1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="301"/>
        <source>UF2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1066"/>
        <source>Click to %1 this session.</source>
        <translation>點擊以 %1 這個療程.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1082"/>
        <source>%1 Session #%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1083"/>
        <source>%1h %2m %3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1114"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1249"/>
        <source>SpO2 Desaturations</source>
        <translation>血氧飽和度降低次數</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1280"/>
        <source>(Mode and Pressure settings missing; yesterday&apos;s shown.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1322"/>
        <source>%1%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1649"/>
        <source>10 of 10 Event Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1708"/>
        <source>This CPAP machine does NOT record detailed data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1801"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;這裡啥都沒有！&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2715"/>
        <source>10 of 10 Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1288"/>
        <source>Awesome</source>
        <translation>美賣，讚喔</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1250"/>
        <source>Pulse Change events</source>
        <translation>脈搏變化重點事件</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1251"/>
        <source>SpO2 Baseline Used</source>
        <translation>血氧飽和度採用基準</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1778"/>
        <source>Zero hours??</source>
        <translation>零小時??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>移至前一天</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="183"/>
        <source>Details</source>
        <translation>詳細說明</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1416"/>
        <source>Time over leak redline</source>
        <translation>漏氣超標的計時</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1066"/>
        <source>disable</source>
        <translation>停用</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1804"/>
        <source>No data is available for this day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2387"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2430"/>
        <source>Bookmark at %1</source>
        <translation>把%1加入書籤</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1317"/>
        <source>Statistics</source>
        <translation>統計值</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>Breakdown</source>
        <translation>解析</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1037"/>
        <source>Unknown Session</source>
        <translation>不明療程</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1774"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>此日存有療程，但已被切換為關閉狀態。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1271"/>
        <source>Model %1 - %2</source>
        <translation>模式 %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1046"/>
        <source>Duration</source>
        <translation>持續時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>檢視尺寸大小</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1777"/>
        <source>Impossibly short session</source>
        <translation>療程太短無法採用</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1569"/>
        <source>Show/hide available graphs.</source>
        <translation>顯示或隱藏可用的圖表.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="632"/>
        <source>No %1 events are recorded this day</source>
        <translation>此日期無%1重點事件記錄</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>顯示或隱藏日曆</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1426"/>
        <source>Time outside of ramp</source>
        <translation>斜線升壓的除外時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1504"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>無法在此系統上顯示圓形圖</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1422"/>
        <source>Total ramp time</source>
        <translation>斜線升壓總時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1396"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>此日期只包含摘要數據資料，而且僅有少量可用資訊。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="364"/>
        <source>Time at Pressure</source>
        <translation>壓力時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>移至次日</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="796"/>
        <source>Session Start Times</source>
        <translation>療程啟動次數</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>結束</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>結束:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>快速變化範圍:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>日常</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>事件</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="129"/>
        <source>Last Fortnight</source>
        <translation>最近兩週</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="61"/>
        <location filename="../oscar/exportcsv.cpp" line="123"/>
        <source>Most Recent Day</source>
        <translation>最近一天</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> 計數</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>檔案名稱:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>Select file to export to</source>
        <translation>選取檔案匯出到</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>分辨率:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>日期:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="109"/>
        <source>Custom</source>
        <translation>自訂</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>匯出</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>开始:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation>資料/時長</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="89"/>
        <source>CSV Files (*.csv)</source>
        <translation>CSV檔案(*.ccsv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="132"/>
        <source>Last Month</source>
        <translation>上個月</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="135"/>
        <source>Last 6 Months</source>
        <translation>最近六個月</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>總時長</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>日期時間</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="74"/>
        <source>OSCAR_</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>療程計數</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation type="unfinished">呼吸中止指數</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="222"/>
        <source>%1% </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>療程</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="120"/>
        <source>Everything</source>
        <translation>所有</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="126"/>
        <source>Last Week</source>
        <translation>上週</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="138"/>
        <source>Last Year</source>
        <translation>去年</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>匯出為CSV格式</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="79"/>
        <source>Sessions_</source>
        <translation>療程_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>詳細資料</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="81"/>
        <source>Summary_</source>
        <translation>概要_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="77"/>
        <source>Details_</source>
        <translation>詳細資料_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>療程</translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="233"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>無法在此個人檔案中匯入此设备的记录。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="232"/>
        <source>Import Error</source>
        <translation>匯入出错</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="233"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>本日的資料已覆蓋已儲存的内容.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>No</source>
        <translation>不</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="112"/>
        <source>Index</source>
        <translation>索引</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="242"/>
        <source>clear</source>
        <translation>清除</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="85"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>帮助引擎未正确設定</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="69"/>
        <source>Help files do not appear to be present.</source>
        <translation>帮助檔案不存在。</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="100"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>帮助引擎無法正确注册檔案。</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="59"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>帮助檔案尚不可用於%1並將顯示在%2。</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="91"/>
        <source>Hide this message</source>
        <translation>隐藏此資訊</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="214"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>無可用檔案</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="116"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Contents</source>
        <translation>目录</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="126"/>
        <source>No documentation available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 结果 &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="196"/>
        <source>Search Topic:</source>
        <translation>搜索主题:</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="166"/>
        <source>Could not find the oximeter file:</source>
        <translation>歹勢，找不到血氧儀檔案:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="172"/>
        <source>Could not open the oximeter file:</source>
        <translation>歹勢，無法開啟血氧儀檔案:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2993"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1629"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="911"/>
        <source>Please insert your CPAP data card...</source>
        <translation>請插入CPAP資料卡...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3243"/>
        <source>Daily Calendar</source>
        <translation>日历</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2916"/>
        <source>&amp;Data</source>
        <translation>&amp;資料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2832"/>
        <source>&amp;File</source>
        <translation>&amp;檔案</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2886"/>
        <source>&amp;Help</source>
        <translation>&amp;帮助</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2857"/>
        <source>&amp;View</source>
        <translation>&amp;查看</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="110"/>
        <source>E&amp;xit</source>
        <translation>&amp;退出</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1441"/>
        <source>Daily</source>
        <translation>日常</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="522"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>載入個人檔案&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3159"/>
        <source>Import &amp;ZEO Data</source>
        <translation>匯入&amp;ZEO資料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2248"/>
        <source>MSeries Import complete</source>
        <translation>M系列PAP資料匯入完成</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1474"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>錯誤資訊截圖儲存在 &quot;%1&quot;檔案中</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="561"/>
        <source>%1 (Profile: %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="918"/>
        <source>Choose a folder</source>
        <translation>選取一個資料夾</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="997"/>
        <source>No profile has been selected for Import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1046"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>%1檔案配置的%2位置在:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1087"/>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1103"/>
        <source>Find your CPAP data card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1162"/>
        <source>Importing Data</source>
        <translation>正在匯入資料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3108"/>
        <source>Online Users &amp;Guide</source>
        <translation>在線&amp;指南</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3020"/>
        <source>View &amp;Welcome</source>
        <translation>查看&amp;欢迎</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3137"/>
        <source>Show Right Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3151"/>
        <source>Show Statistics view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3164"/>
        <source>Import &amp;Dreem Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3194"/>
        <source>Import &amp;Viatom/Wellue Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3207"/>
        <source>Show &amp;Line Cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3229"/>
        <source>Show Daily Left Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3246"/>
        <source>Show Daily Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3262"/>
        <source>Show Performance Information</source>
        <translation>顯示性能資訊</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2244"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>開啟M系列PAP檔案出错: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3199"/>
        <source>Current Days</source>
        <translation>当前天数</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="107"/>
        <source>&amp;About</source>
        <translation>&amp;关於</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2998"/>
        <source>View &amp;Daily</source>
        <translation>查看&amp;日常</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3009"/>
        <source>View &amp;Overview</source>
        <translation>查看&amp;概述</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1358"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>重新計算完成之前，已阻止存取首選项。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3169"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>匯入瑞斯迈&amp;M系列PAP資料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1861"/>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation>由於某種原因，OSCAR没有以下设备的任何備份:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3226"/>
        <source>Daily Sidebar</source>
        <translation>每日侧边栏</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1929"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>請注意：請將備份資料夾保留在合适的位置。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1996"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>檔案权限錯誤導致清除过程失败; 您必須手動删除以下資料夾:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3123"/>
        <source>Change &amp;User</source>
        <translation>變更&amp;使用者</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2569"/>
        <source>%1&apos;s Journal</source>
        <translation>%1&apos;的日誌</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="746"/>
        <source>Import Problem</source>
        <translation>匯入錯誤</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2485"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;請注意，您無法撤消此操作!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3145"/>
        <source>View S&amp;tatistics</source>
        <translation>查看&amp;統計值</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1013"/>
        <source>Monthly</source>
        <translation>每月</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3179"/>
        <source>Change &amp;Language</source>
        <translation>變更&amp;语言</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3044"/>
        <source>&amp;About OSCAR</source>
        <translation>&amp;关於OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1579"/>
        <source>Import</source>
        <translation>匯入</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1879"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>由於没有可用的内部備份可供重建使用，請自行從備份中还原。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1880"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation>您希望立即從備份匯入吗？(完成匯入，才能有資料顯示)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="810"/>
        <location filename="../oscar/mainwindow.cpp" line="1876"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>請稍等,正在由備份資料夾匯入...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1415"/>
        <source>Check for updates not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1464"/>
        <source>Choose where to save screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1464"/>
        <source>Image files (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1538"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1854"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>請注意，如果停用了OSCAR&apos;s備份，這可能會導致資料丢失。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1931"/>
        <source>OSCAR does not have any backups for this machine!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1932"/>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this machine&lt;/i&gt;, &lt;font size=+2&gt;you will lose this machine&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2259"/>
        <source>The Glossary will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2347"/>
        <location filename="../oscar/mainwindow.cpp" line="2351"/>
        <source>There was a problem opening %1 Data File: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2350"/>
        <source>%1 Data Import of %2 file(s) complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2352"/>
        <source>%1 Import Partial Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2354"/>
        <source>%1 Data Import complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2483"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>確定清除%1内的血氧儀資料吗</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2810"/>
        <source>OSCAR Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3090"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>&amp;血氧儀安裝小幫手</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1751"/>
        <source>Bookmarks</source>
        <translation>標記簇</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3134"/>
        <source>Right &amp;Sidebar</source>
        <translation>右&amp;侧边栏</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2952"/>
        <source>Rebuild CPAP Data</source>
        <translation>重建資料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1546"/>
        <source>The FAQ is not yet implemented</source>
        <translation>FAQ尚未施行</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2571"/>
        <source>XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2591"/>
        <source>Export review is not yet implemented</source>
        <translation>匯出检查不可用</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1851"/>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation>確定要重建以下设备的所有CPAP資料吗:

</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3292"/>
        <source>Report an Issue</source>
        <translation>報告问题</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1020"/>
        <source>Date Range</source>
        <translation>日期範圍</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3148"/>
        <source>View Statistics</source>
        <translation>查看統計值資訊</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1053"/>
        <source>CPAP Data Located</source>
        <translation>CPAP資料已定位</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1001"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>匯入資料存取被阻止，重新計算進行中。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3174"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>睡眠障碍术语&amp;术语表</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1864"/>
        <source>Are you really sure you want to do this?</source>
        <translation>確定進行此操作?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2515"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>請先在每日视图中選取有效血氧儀資料的日期.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2924"/>
        <source>Purge Oximetry Data</source>
        <translation>清除血氧测定資料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2773"/>
        <source>Records</source>
        <translation>記錄</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3039"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>使用&amp;圖形保真</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1013"/>
        <source>Would you like to import from this location?</source>
        <translation>從此匯入吗?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="996"/>
        <source>Report Mode</source>
        <translation>報告模式</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2988"/>
        <source>&amp;Profiles</source>
        <translation>&amp;個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1347"/>
        <source>Profiles</source>
        <translation>個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3267"/>
        <source>Create zip of CPAP data card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3272"/>
        <source>Create zip of OSCAR diagnostic logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3277"/>
        <source>Create zip of all OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3282"/>
        <source>CSV Export Wizard</source>
        <translation>CSV匯出小幫手</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3118"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>&amp;血氧儀資料自動清理</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1005"/>
        <source>Import is already running in the background.</source>
        <translation>已在后台执行匯入操作.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1057"/>
        <source>Specify</source>
        <translation>指定</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1003"/>
        <location filename="../oscar/mainwindow.ui" line="3319"/>
        <source>Standard</source>
        <translation>標準</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2046"/>
        <source>No help is available.</source>
        <translation>没有可用的帮助。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1391"/>
        <source>Statistics</source>
        <translation>統計值</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="744"/>
        <source>Up to date</source>
        <translation>最新</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1353"/>
        <source>Please open a profile first.</source>
        <translation>請先開啟個人檔案.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="942"/>
        <source>&amp;Statistics</source>
        <translation>&amp;統計值</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3254"/>
        <source>Backup &amp;Journal</source>
        <translation>備份&amp;日誌</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="742"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>已匯入 %1 PAP 療程，來源機器為

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2601"/>
        <source>Would you like to zip this card?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2623"/>
        <location filename="../oscar/mainwindow.cpp" line="2694"/>
        <location filename="../oscar/mainwindow.cpp" line="2745"/>
        <source>Choose where to save zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2623"/>
        <location filename="../oscar/mainwindow.cpp" line="2694"/>
        <location filename="../oscar/mainwindow.cpp" line="2745"/>
        <source>ZIP files (*.zip)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2670"/>
        <location filename="../oscar/mainwindow.cpp" line="2708"/>
        <location filename="../oscar/mainwindow.cpp" line="2779"/>
        <source>Creating zip...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2655"/>
        <location filename="../oscar/mainwindow.cpp" line="2763"/>
        <source>Calculating size...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2801"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>報告问题不可用</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2935"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>清除&amp;当前所选日期的資料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1863"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>如果已经為所有CPAP資料進行了&lt;i&gt;備份 &lt;b&gt;，&lt;/b&gt;仍然可以完成此操作&lt;/i&gt;，但必須手動從備份中还原。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2920"/>
        <source>&amp;Advanced</source>
        <translation>&amp;進階</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3098"/>
        <source>Print &amp;Report</source>
        <translation>列印&amp;報告</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="746"/>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation>此处没有有效的PAP資料

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3287"/>
        <source>Export for Review</source>
        <translation>匯出查看</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3082"/>
        <source>Take &amp;Screenshot</source>
        <translation>&amp;截圖</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1485"/>
        <source>Overview</source>
        <translation>總覽</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2861"/>
        <source>&amp;Reset Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2890"/>
        <source>Troubleshooting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2930"/>
        <source>Purge ALL Machine Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2975"/>
        <source>&amp;Import CPAP Card Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3001"/>
        <source>Show Daily view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3012"/>
        <source>Show Overview view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3052"/>
        <source>&amp;Maximize Toggle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3055"/>
        <source>Maximize window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3066"/>
        <source>Show Debug Pane</source>
        <translation>顯示调试面板</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3074"/>
        <source>Reset Graph &amp;Heights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3077"/>
        <source>Reset sizes of graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3103"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;編輯個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3372"/>
        <source>&amp;Sleep Stage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3377"/>
        <source>&amp;Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3382"/>
        <source>&amp;All except Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3387"/>
        <source>All including &amp;Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1088"/>
        <source>Import Reminder</source>
        <translation>匯入提示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="268"/>
        <source>Help Browser</source>
        <translation>帮助浏览器</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1688"/>
        <location filename="../oscar/mainwindow.cpp" line="1715"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>重启命令不起作用，需要手動重启。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2836"/>
        <source>Exp&amp;ort Data</source>
        <translation>导&amp;出資料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="540"/>
        <location filename="../oscar/mainwindow.cpp" line="2200"/>
        <source>Welcome</source>
        <translation>欢迎使用</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3189"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>匯入&amp;睡眠姿势資料</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1476"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>截圖儲存於 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2983"/>
        <source>&amp;Preferences</source>
        <translation>&amp;参数設定</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1938"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation>您將要&lt;font size=+2&gt;删除以下设备的&lt;/font&gt;OSCAR資料库:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1941"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>您 &lt;b&gt;確定&lt;/b&gt; 要繼續吗?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="742"/>
        <source>Import Success</source>
        <translation>匯入成功</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2571"/>
        <source>Choose where to save journal</source>
        <translation>選取儲存日誌的位置</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3113"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>&amp;常见问题</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1529"/>
        <source>Oximetry</source>
        <translation>血氧测定</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3297"/>
        <source>System Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3308"/>
        <source>Show &amp;Pie Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3311"/>
        <source>Show Pie Chart on Daily page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3314"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3322"/>
        <source>Standard graph order, good for CPAP, APAP, Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3327"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3330"/>
        <source>Advanced graph order, good for ASV, AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3341"/>
        <source>Show Personal Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3349"/>
        <source>Check For &amp;Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3357"/>
        <source>Purge Current Selected Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3362"/>
        <source>&amp;CPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3367"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;血氧测量</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1048"/>
        <source>A %1 file structure was located at:</source>
        <translation>%1 檔案配置的位置在:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3184"/>
        <source>Change &amp;Data Folder</source>
        <translation>變更&amp;資料資料夾</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1320"/>
        <source>Navigation</source>
        <translation>导航</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="744"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>已更新CPAP資料位於

%1</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2049"/>
        <source>Scaling Mode</source>
        <translation>缩放模式</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2014"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>Y轴最大值,必須大於最小值方可正常工作.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2071"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>此按钮將按自适应模式重新設定最大最小值</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2007"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Y轴缩放模式:“自适应”意味着自動适应大小，“預設”意味着使用制造商的出厂值，“覆蓋”意味着自訂.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2013"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>Y轴最小值，此值可為负。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2005"/>
        <source>Defaults</source>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2004"/>
        <source>Auto-Fit</source>
        <translation>自适应</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2006"/>
        <source>Override</source>
        <translation>覆蓋</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="654"/>
        <source>ASV</source>
        <translation>适应性支持同期模式</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="644"/>
        <source>APAP</source>
        <translation>全自動正压通氣</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="639"/>
        <source>CPAP</source>
        <translation>持续氣道正压通氣</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="399"/>
        <source>Male</source>
        <translation>男</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="152"/>
        <source>Very weak password protection and not recommended if security is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>D.O.B.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="459"/>
        <source>Metric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="464"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="970"/>
        <source>&amp;Back</source>
        <translation>&amp;上一步</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="986"/>
        <location filename="../oscar/newprofile.cpp" line="276"/>
        <location filename="../oscar/newprofile.cpp" line="285"/>
        <source>&amp;Next</source>
        <translation>&amp;下一步</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="253"/>
        <source>TimeZone</source>
        <translation>時区</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="521"/>
        <location filename="../oscar/newprofile.ui" line="810"/>
        <source>Email</source>
        <translation>电子邮件</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="531"/>
        <location filename="../oscar/newprofile.ui" line="800"/>
        <source>Phone</source>
        <translation>电话</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="124"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>所有生成的報告仅限個人使用，不能用於医疗诊断。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="134"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019-2022 The OSCAR Team</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="452"/>
        <source>&amp;Close this window</source>
        <translation>&amp;關閉窗口</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>編輯使用者資訊</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="127"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>该應用程式作者对&lt;u&gt;任何人&lt;/u&gt; 使用或误用本應用程式不承担任何责任。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="157"/>
        <source>Please provide a username for this profile</source>
        <translation>請输入使用者名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="576"/>
        <source>CPAP Treatment Information</source>
        <translation>PAP治療資訊</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Password Protect Profile</source>
        <translation>密碼保护</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="119"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR 仅仅作為一個資料读取顯示應用程式，不能替代医生提供有效的医疗指导。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>欢迎使用开源CPAP分析報告</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR已根据&lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU公共许可证免费发布v3版本&lt;/a&gt;，没有任何担保，也没有任何针对任何目的的适用性声明。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>不保证任何顯示資料的准确性。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="404"/>
        <source>Female</source>
        <translation>女</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>Gender</source>
        <translation>性别</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="420"/>
        <source>Height</source>
        <translation>身高</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="477"/>
        <source>Contact Information</source>
        <translation>联系方式</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="213"/>
        <source>Locale Settings</source>
        <translation>归属地設定</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="631"/>
        <source>CPAP Mode</source>
        <translation>CPAP模式</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="63"/>
        <source>Select Country</source>
        <translation>選取国家</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="118"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>請认真阅读</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="617"/>
        <source>Untreated AHI</source>
        <translation>未治療時的AHI</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="504"/>
        <location filename="../oscar/newprofile.ui" line="779"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="70"/>
        <source>I agree to all the conditions above.</source>
        <translation>同意以上条款。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="240"/>
        <source>DST Zone</source>
        <translation>DST時区</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>关於：空白</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="662"/>
        <source>RX Pressure</source>
        <translation>释放壓力</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="182"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="131"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>后果自负。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="167"/>
        <source>Passwords don&apos;t match</source>
        <translation>密碼不匹配</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="344"/>
        <source>First Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="354"/>
        <source>Last Name</source>
        <translation>姓氏</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="285"/>
        <source>Country</source>
        <translation>国家</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="954"/>
        <source>&amp;Cancel</source>
        <translation>&amp;取消</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="274"/>
        <source>&amp;Finish</source>
        <translation>&amp;完成</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="649"/>
        <source>Bi-Level</source>
        <translation>双水平</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Profile Changes</source>
        <translation>個人檔案變更</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="320"/>
        <source>Personal Information (for reports)</source>
        <translation>個人資訊</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="137"/>
        <source>User Name</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;可以留空或者跳过這一步，但提供出生日期和性别可以提高計算的准确性。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="113"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation>這款應用程式用於协助读取用於治療睡眠障碍的各种CPAP的資料.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="111"/>
        <source>User Information</source>
        <translation>使用者資訊</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="196"/>
        <source>...twice...</source>
        <translation>...两次...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="735"/>
        <source>Doctors Name</source>
        <translation>医生姓名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="364"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>可以跳过這一步，但提供大概年龄資料可以提高計算的准确性。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="708"/>
        <source>Doctors / Clinic Information</source>
        <translation>医生/诊所資訊</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="752"/>
        <source>Practice Name</source>
        <translation>患者姓名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="603"/>
        <source>Date Diagnosed</source>
        <translation>诊断日期</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Accept and save this information?</source>
        <translation>接收並保存此資訊?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="762"/>
        <source>Patient ID</source>
        <translation>患者编号</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="158"/>
        <source>End:</source>
        <translation>結束:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="190"/>
        <source>Usage</source>
        <translation>使用數據</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="182"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>呼吸
紊乱
指數</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="359"/>
        <source>10 of 10 Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="645"/>
        <source>Show all graphs</source>
        <translation>顯示所有圖表</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="181"/>
        <source>Reset view to selected date range</source>
        <translation>將视图重新設定為所选日期範圍</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="201"/>
        <source>Total Time in Apnea</source>
        <translation>呼吸中止總時間</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="265"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>下拉以查看要開啟/關閉的圖表列表。</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="190"/>
        <source>Usage
(hours)</source>
        <translation>使用
(小時）</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>前三個月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="201"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>呼吸中止總時間
（分鐘）</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>自訂</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="245"/>
        <source>How you felt
(0-10)</source>
        <translation>感觉如何
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="272"/>
        <source>Graphs</source>
        <translation>圖表</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>範圍:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="135"/>
        <source>Start:</source>
        <translation>开始:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>上個月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="184"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>中止
低通氣
指數</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>前六個月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="240"/>
        <source>Body
Mass
Index</source>
        <translation>身体
重量
指數</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="193"/>
        <source>Session Times</source>
        <translation>療程次數</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>前兩週</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>所有</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>上週</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>去年</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="127"/>
        <source>Snapshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="249"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="230"/>
        <source>Toggle Graph Visibility</source>
        <translation>切换视图</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="658"/>
        <source>Hide all graphs</source>
        <translation>隐藏所有圖表</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>前两個月</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1013"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;請激活血氧儀标识符，以区分多個血氧儀&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="536"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>实時血氧测量匯入已停止</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1344"/>
        <source>Press Start to commence recording</source>
        <translation>开始记录</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="330"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="591"/>
        <source>No CPAP data available on %1</source>
        <translation>在%1中没有可用的CPAP資料</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="599"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1148"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>还可以读取ChoiceMMed MD300W1血氧儀的 .dat檔案.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="369"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>請等待血氧儀上传資料結束，期間不要拔出血氧儀.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="733"/>
        <source>Finger not detected</source>
        <translation>没有檢測到手指</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>請在血氧儀操作开始上传資料到电脑.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="435"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>血氧儀無法解析所选檔案：</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="257"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>正在為血氧儀從 &apos;%1&apos; 改名到 &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="823"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;請注意: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;請确保血氧儀類型選取正确無误，否则會匯入失败.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="921"/>
        <source>CMS50Fv3.7+/H/I, CMS50D+v4.6, Pulox PO-400/500</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="926"/>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="931"/>
        <source>ChoiceMMed MD300W1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1035"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1076"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1105"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1186"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;开启這個功能则允许由資料資料夾匯入入SpO2Review這样的脈搏血氧儀记录的读数.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="388"/>
        <source>Oximeter import completed..</source>
        <translation>血氧儀資料匯入完成..</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2130"/>
        <source>&amp;Retry</source>
        <translation>&amp;再试一次</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2225"/>
        <source>&amp;Start</source>
        <translation>&amp;开始</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1145"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>你可能注意到，其他的公司，比如Pulox， Pulox PO-200, PO-300, PO-400.也可以使用.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="587"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 療程 %2, 開始時間為 %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="836"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>血氧儀没有内置時鐘，需要手動設定。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1993"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>如果有需要，可以在此手動调整時間:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1141"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>OSCAR能够在CPAP療程資料的同時跟踪血氧测定資料，從而对CPAP治療的有效性提供有价值的见解。它也將與脈搏血氧儀独立工作，允许儲存，跟踪和审查记录資料。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1096"/>
        <source>Oximeter Session %1</source>
        <translation>血氧儀療程 %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="472"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>無法與血氧儀连通</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1643"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>請選取哪個要匯入OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1271"/>
        <source>Please connect your oximeter device</source>
        <translation>請连接血氧儀</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1154"/>
        <source>Important Notes:</source>
        <translation>重要提示:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="486"/>
        <source>Starting up...</source>
        <translation>开始...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1157"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Contec CMS50D+没有内部時鐘，所以不能够记录开始時間。如果PAP資料與其無法同步，請在匯入完成后手動输入开始時間.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2014"/>
        <source>HH:mm:ssap</source>
        <translation>小時:分鐘:秒</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1111"/>
        <source>Import directly from a recording on a device</source>
        <translation>直接由磁盘匯入</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="260"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>血氧儀名稱不同...如果您仅有一個血氧儀而且與不用的使用者公用，請將其名稱统一.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1159"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>即使对於含有内部時鐘的血氧儀，仍然建議养成血氧儀與PAP同時开启记录的习惯，因為PAP的内部時鐘會存在漂移现象，而且不易复位.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1318"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1951"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2092"/>
        <source>&amp;Information Page</source>
        <translation>&amp;資訊页</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1775"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>使用血氧儀的時間作為系统時鐘.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="535"/>
        <source>Live Oximetry Stopped</source>
        <translation>实時血氧测量已停止</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Waiting for %1 to start</source>
        <translation>等待 %1 开始</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1143"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR目前可以兼容Contec CMS50D+、CMS50E、CMS50F和CMS50I系列血氧儀。&lt;br/&gt;(請注意：直接從蓝牙模式匯入是不可行的 &lt;span style=&quot; font-weight:600;&quot;&gt;&lt;/span&gt; )</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1236"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;給 PAP 使用者的提示： &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;請務必先行匯入呼吸器資料&lt;br/&gt;&lt;/span&gt;否則血氧儀資料將無法與呼吸器資料在時間軸上同步。&lt;br/&gt;請同時啟動兩台機器，以確保資料的同步完整。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1454"/>
        <source>SpO2 %</source>
        <translation>血氧飽和度 %</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="413"/>
        <source>Select a valid oximetry data file</source>
        <translation>選取一個可用的血氧儀資料檔案</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="368"/>
        <source>%1 device is uploading data...</source>
        <translation>%1设备正在上传資料...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2168"/>
        <source>&amp;End Recording</source>
        <translation>&amp;停止记录</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="966"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>CMS50E/F使用者，在直接匯入時，請不要在设备上選取上传，直到OSCAR提示您為止。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2149"/>
        <source>&amp;Choose Session</source>
        <translation>&amp;選取療程</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Nothing to import</source>
        <translation>没有可匯入的資料</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="336"/>
        <source>Select upload option on %1</source>
        <translation>在%1選取上传選項</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="910"/>
        <source>Select Oximeter Type:</source>
        <translation>選取血氧儀類型:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="334"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>正在等待设备开始上传資料...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="221"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>没有连接血氧儀设备.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="38"/>
        <source>Oximeter Import Wizard</source>
        <translation>血氧儀匯入小幫手</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2206"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;儲存並結束</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1139"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>脉動血氧儀是一款用於测量血样飽和度的医疗设备,在呼吸中止以及低通氣事件发生時，血氧飽和度大幅降低會引起一系列的健康问题，需要引起重视。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1155"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>為了使OSCAR能够直接從血氧儀设备上定位和读取，需要确保在計算机上安装了正确的设备驱動程序（如USB转串行UART）。有关更多資訊%1，請點擊此%2.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1666"/>
        <source>Start Time</source>
        <translation>开始時間</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1820"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR需要一個开始時間来知道將血氧儀療程保存到哪里。&lt;/p&gt;&lt;p&gt;選取以下選項之一：&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1535"/>
        <source>Pulse Rate</source>
        <translation>脈搏</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1852"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;注意:同步CPAP療程的起始時間往往更加准确.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1006"/>
        <source>Set device date/time</source>
        <translation>設定日期/時間</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="413"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>血氧儀檔案 (*.spo *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1693"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>匯入完成.何時开始记录?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1192"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>匯入其他程序創建的資料檔案，例如SpO2Review所創建的檔案</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1717"/>
        <source>Day recording (normally would of) started</source>
        <translation>日常记录开启</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1627"/>
        <source>Multiple Sessions Detected</source>
        <translation>檢測到多重療程</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1791"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation>开启血氧儀记录的時間和开启CPAP的時間一致（或相近）.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1153"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>整晚连入电脑记录(提供体描仪)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1079"/>
        <source>Erase session after successful upload</source>
        <translation>上传成功后删除療程</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="483"/>
        <source>Live Oximetry Mode</source>
        <translation>实時血氧测量模式</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2111"/>
        <source>&amp;Cancel</source>
        <translation>&amp;取消</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1016"/>
        <source>Set device identifier</source>
        <translation>設定设备标识符</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1676"/>
        <source>Details</source>
        <translation>詳細資料</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1003"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;如果啟用，OSCAR將使用您的計算机当前時間自動重新設定CMS50的内部時鐘。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="465"/>
        <source>Oximeter not detected</source>
        <translation>未檢測到血氧儀</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1150"/>
        <source>Please remember:</source>
        <translation>請谨记:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="866"/>
        <source>Where would you like to import from?</source>
        <translation>從何处匯入資料?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1289"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>如果您看到此处提示，請重新設定血氧儀類型.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="833"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>希望使用电脑的時間作為实時血氧療程的時間.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="189"/>
        <source>Scanning for compatible oximeters</source>
        <translation>正在扫描所兼容的血氧儀</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="338"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>請连接血氧儀，點擊選單選取資料上传...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1873"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>選取CPAP療程同步於:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1419"/>
        <location filename="../oscar/oximeterimport.ui" line="1671"/>
        <source>Duration</source>
        <translation>時長</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1137"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>欢迎使用血氧儀資料匯入小幫手</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1147"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;如果你不介意整晚连入电脑，可以生成容积图，可以直观的展现心率，顯示在常规的血氧读数顶端.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1152"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>如果您嘗試同步血氧测定和CPAP資料，請确保在繼續之前先匯入您的CPAP療程！</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="303"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, %2療程</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1389"/>
        <source>Show Live Graphs</source>
        <translation>顯示实時圖表</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="534"/>
        <source>Live Import Stopped</source>
        <translation>实時匯入已停止</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1763"/>
        <source>Oximeter Starting time</source>
        <translation>血氧儀开启時間</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="768"/>
        <source>Skip this page next time.</source>
        <translation>下次跳过此页面。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="487"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>如果在几秒鐘后仍然可以阅读此内容，請取消並重试</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2187"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;同步並儲存</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>血氧儀療程無效.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="848"/>
        <source>Something went wrong getting session data</source>
        <translation>获取療程資料時出错</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="229"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>正在與%1血氧儀连接</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="726"/>
        <source>Recording...</source>
        <translation>正在儲存...</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>脈搏</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>&amp;開啟 SPO/R 檔案</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>重&amp;置</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>序列号&amp;匯入</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>产口</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>日/月/年 小時:分鐘:秒</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>&amp;开始</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;扫描端口</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3181"/>
        <source>&amp;Ok</source>
        <translation>&amp;好的</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1212"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>不建議關閉備份，因為如果发现錯誤，OSCAR需要這些備份来重建資料库。

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2383"/>
        <source>Graph Height</source>
        <translation>圖表高度</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Flag</source>
        <translation>標記</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2820"/>
        <source>Font</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="461"/>
        <location filename="../oscar/preferencesdialog.cpp" line="592"/>
        <source>Name</source>
        <translation>姓名</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2838"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Span</source>
        <translation>範圍</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1980"/>
        <source>General Settings</source>
        <translation>通用設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2597"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;在使用双向触摸板放大時，滚動顯示更容易&lt;/p&gt;&lt;p&gt;50ms是推荐值.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="462"/>
        <location filename="../oscar/preferencesdialog.cpp" line="593"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2316"/>
        <location filename="../oscar/preferencesdialog.ui" line="2355"/>
        <source>Daily</source>
        <translation>日常</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1164"/>
        <source>Event Duration</source>
        <translation>事件区間</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="785"/>
        <source>Hours</source>
        <translation>小時</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="465"/>
        <location filename="../oscar/preferencesdialog.cpp" line="597"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="595"/>
        <source>Lower</source>
        <translation>更低</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="294"/>
        <source>Never</source>
        <translation>從不</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1548"/>
        <source>Oximetry Settings</source>
        <translation>血氧飽和度設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1734"/>
        <source>Pulse</source>
        <translation>脈搏</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2045"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>圖形引擎</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="596"/>
        <source>Upper</source>
        <translation>更高</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2143"/>
        <source>days.</source>
        <translation>天.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="272"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;療程將會比這個稍短並且不會顯示&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="684"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>在此可以為%1波形設定&lt;b&gt;更高的&lt;/b&gt; 閥值来進行某些計算</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2373"/>
        <source>After Import</source>
        <translation>匯入后</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="245"/>
        <source>Ignore Short Sessions</source>
        <translation>忽略短時療程</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="618"/>
        <source>Sleep Stage Waveforms</source>
        <translation>睡眠階段波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1104"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>氣流限值的中值百分比
20%的氣流限值有利於檢測呼吸中止。 </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="326"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>在此之前开始一段療程將會计入上一天.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="399"/>
        <source>Session Storage Options</source>
        <translation>療程儲存選項</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1199"/>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation>顯示已標記但仍未被识别的事件.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3015"/>
        <source>Graph Titles</source>
        <translation>圖表标题</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1274"/>
        <source>Zero Reset</source>
        <translation>归零</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1081"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;自訂標記是一個檢測被機器忽略的事件实验方法。它们&lt;span style=&quot; text-decoration: underline;&quot;&gt;不&lt;/span&gt;包含於 AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="795"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>应用這些變更需要資料重新/解压缩过程。此操作可能需要几分鐘才能完成。

确实要進行這些變更吗？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="972"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>激活/停用（实验性）突出事件標記。
允许檢測边缘事件以及设备遗漏事件
這個選項必須在匯入前激活，否则需要清除缓存。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1063"/>
        <source>Flow Restriction</source>
        <translation>氣流限制</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1202"/>
        <source>Enable Unknown Events Channels</source>
        <translation>啟用位置事件通道</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1686"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>血氧飽和下降的最小区間</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2652"/>
        <source>Overview Linecharts</source>
        <translation>線形图概览</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2731"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>是否允许以双击Y轴来進行Y轴的缩放</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="53"/>
        <source>Always Minor</source>
        <translation>保持小</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="66"/>
        <source>No CPAP machines detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="67"/>
        <source>Will you be using a ResMed brand machine?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="321"/>
        <location filename="../oscar/preferencesdialog.cpp" line="322"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1284"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1289"/>
        <source>%1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="486"/>
        <source>Unknown Events</source>
        <translation>未知事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2701"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>像素映射缓存是圖形加速技术,或许會導致在您的操作系统上的字体顯示异常.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1861"/>
        <location filename="../oscar/preferencesdialog.ui" line="1940"/>
        <source>Reset &amp;Defaults</source>
        <translation>恢复&amp;預設設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="580"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>跳过使用者登录界面，登录常用使用者</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="803"/>
        <source>Data Reindex Required</source>
        <translation>重建資料索引</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="74"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;請注意：&lt;/b&gt;OSCAR的進階療程分割功能由於其設定和摘要資料的儲存方式的限制而無法用於ResMed设备，因此它们已针对该個人檔案被停用。&lt;/p&gt;&lt;p&gt;在ResMed设备上，日期將在中午分开，和在ResMed的商业應用程式的設定相同。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2479"/>
        <source>Scroll Dampening</source>
        <translation>滚動抑制</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1290"/>
        <source> L/min</source>
        <translation> 升/分鐘</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1168"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> 确实要停用這些備份吗？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1257"/>
        <source>Flag leaks over threshold</source>
        <translation>漏氣超閥值标志</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1309"/>
        <source> hours</source>
        <translation> 小時</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="658"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>双击變更這個通道的描述。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="514"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>將不會匯入早於此日期的療程</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1628"/>
        <source>Flag SPO2 Desaturations Below</source>
        <translation>SPO2去飽和度標記低</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2430"/>
        <source>Standard Bars</source>
        <translation>標準导航条</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1457"/>
        <source>99% Percentile</source>
        <translation>99%百分位数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="574"/>
        <source>Memory and Startup Options</source>
        <translation>儲存與啟動選項</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="630"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;在匯入期間减少任何不重要的确认对话框。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1598"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>少量的血氧测定資料將被丢弃。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="616"/>
        <source>Oximeter Waveforms</source>
        <translation>血氧儀波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1287"/>
        <source>User definable threshold considered large leak</source>
        <translation>使用者自訂大量漏氣数值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1271"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>在每個窗口開啟時將计数器归零。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1216"/>
        <source>Compliance defined as</source>
        <translation>符合性定义為</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="613"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;通过預先載入所有摘要資料，可以使啟動OSCAR的速度稍慢一些，這样可以加快浏览概述和稍后的其他一些計算。&lt;/p&gt;&lt;p&gt;如果有大量資料，建議關閉此项&lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; 總而言之，仍然必須載入所有摘要資料。&lt;/p&gt;&lt;p&gt;注意：该設定不會影响波形和事件資料，根据需要進行載入。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="551"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>在此可以變更事件顯示的標記類型</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2435"/>
        <source>Top Markers</source>
        <translation>置顶标志</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="811"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>您所做的一個或多個變更將要求重新啟動此应用程序，以便這些變更生效。

確定進行此操作吗？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1233"/>
        <source> minutes</source>
        <translation> 分鐘</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="170"/>
        <location filename="../oscar/preferencesdialog.ui" line="255"/>
        <location filename="../oscar/preferencesdialog.ui" line="758"/>
        <source>Minutes</source>
        <translation>分鐘</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="489"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>在匯入过程中創建SD卡備份（請自行關閉此功能！）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2283"/>
        <source>Graph Settings</source>
        <translation>圖形設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="556"/>
        <location filename="../oscar/preferencesdialog.cpp" line="689"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>這是將在屏幕所顯示的此通道的简短描述标签.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="458"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>以下選項會影响OSCAR使用的磁盘空間量，並影响匯入的時間。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="482"/>
        <source>CPAP Events</source>
        <translation>CPAP 事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2856"/>
        <source>Bold  </source>
        <translation>突出顯示  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2299"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;載入個人檔案時要開啟哪個選項卡。（注意：如果OSCAR設定為在啟動時不開啟個人檔案，它將預設為個人檔案）&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1715"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>脈搏變更事件的最小区間。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2677"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>圖形保真技术可以使得圖表顯示更加圆润
当這一功能啟用時，特定的图块會突出顯示
在列印報告中也會体现出来

可以進行嘗試。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="485"/>
        <source>Sleep Stage Events</source>
        <translation>睡眠階段事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1808"/>
        <source>Events</source>
        <translation>事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="362"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;設定前請注意...&lt;/span&gt;關閉這一選項的后果就是影响彙總報告的准确性，因為某些計算只在某一天的資料保存在一起的時候才能正常工作 . &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;瑞思迈使用者:&lt;/span&gt; 正午12点之前属於前一天，這对你我来说感觉很自然，但不代表瑞思迈也這么认為。STF.edf功能很弱，並不适合来实现這一功能。.&lt;/p&gt;&lt;p&gt;這一選項存在的意义在於安抚那些不在意看到什么，只是想看到 &amp;quot;固定的資料&amp;quot;不管成本，但是這始终是有代价的.如果你每天都记录資料，並且每周匯入电脑一次，不會经常遇到這個报错 .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1414"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>建議瑞斯迈使用者選取中值。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="483"/>
        <source>Oximeter Events</source>
        <translation>血氧儀事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2877"/>
        <source>Italic</source>
        <translation>意大利</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2005"/>
        <source>Enable Multithreading</source>
        <translation>啟用多線程</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1164"/>
        <source>This may not be a good idea</source>
        <translation>不正确的应用</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1423"/>
        <source>Weighted Average</source>
        <translation>平均体重</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1418"/>
        <location filename="../oscar/preferencesdialog.ui" line="1481"/>
        <source>Median</source>
        <translation>中間值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1667"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>血氧儀統計值資料中標記快速變更</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1699"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>脈搏突然變更的最小值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1831"/>
        <location filename="../oscar/preferencesdialog.ui" line="1910"/>
        <source>Search</source>
        <translation>查询</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1027"/>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation>重新同步PAP檢測到的事件(试验性功能)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1471"/>
        <source>Time Weighted average of Indice</source>
        <translation>時間加权平均值指數</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1386"/>
        <source>Middle Calculations</source>
        <translation>中值計算</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2724"/>
        <source>Skip over Empty Days</source>
        <translation>跳过無資料的日期</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1151"/>
        <source>Allow duplicates near machine events.</source>
        <translation>允许多重记录趋近機器事件資料。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2425"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>將视窗顯示的波形的標記進行叠加。
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1400"/>
        <source>Upper Percentile</source>
        <translation>增大</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="810"/>
        <source>Restart Required</source>
        <translation>重启請求</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1254"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>是否在漏氣圖表中顯示漏氣限值红線</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1166"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> 如果您需要再次重新匯入此資料（無论是在OSCAR还是ResScan中），此資料將不會再返回。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1452"/>
        <source>True Maximum</source>
        <translation>真极大值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Minor Flag</source>
        <translation>次要標記</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="794"/>
        <source>Data Processing Required</source>
        <translation>需要資料处理</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1363"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>為了保持一致，ResMed的使用者需要設定95%,
它將作為唯一值出现在彙總界面内。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2289"/>
        <source>On Opening</source>
        <translation>开启状态</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="616"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>啟動時預載入所有彙總資料</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2012"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>OSCAR關閉時顯示删除卡提醒通知</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2345"/>
        <source>No change</source>
        <translation>無變更</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2954"/>
        <source>Graph Text</source>
        <translation>圖表文字</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="536"/>
        <location filename="../oscar/preferencesdialog.cpp" line="665"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>双击變更這個區塊/標記/資料的預設颜色.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1495"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;由於總结设计限制，ResMed機器不支持變更這些設定。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1209"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>AHI/小時 圖形時間窗</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="428"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="633"/>
        <source>Import without asking for confirmation</source>
        <translation>無需确认直接匯入</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="640"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any machine model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="643"/>
        <source>Warn when importing data from an untested machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="650"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="653"/>
        <source>Warn when previously unseen data is encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="664"/>
        <source>&amp;CPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="824"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="880"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="911"/>
        <source>4 cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="921"/>
        <source>20 cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1004"/>
        <location filename="../oscar/preferencesdialog.ui" line="1128"/>
        <location filename="../oscar/preferencesdialog.ui" line="1601"/>
        <location filename="../oscar/preferencesdialog.ui" line="1689"/>
        <location filename="../oscar/preferencesdialog.ui" line="1718"/>
        <source>s</source>
        <translation type="unfinished">秒s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1024"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1332"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation type="unfinished">呼吸中止指數</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1337"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation type="unfinished">呼吸紊乱指數</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1591"/>
        <location filename="../oscar/preferencesdialog.ui" line="1621"/>
        <location filename="../oscar/preferencesdialog.ui" line="1702"/>
        <source> bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1611"/>
        <source>Discard segments under</source>
        <translation>删除偏低的資料</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2001"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>允许使用多核CPU以提高性能
提高匯入性能。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2019"/>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2073"/>
        <source>Check For Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2088"/>
        <source>You are using a test version of OSCAR. Test versions check for updates automatically at least once every seven days.  You may set the interval to less than seven days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2105"/>
        <source>Automatically check for updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2127"/>
        <source>How often OSCAR should check for updates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2208"/>
        <source>If you are interested in helping test new features and bugfixes early, click here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2224"/>
        <source>If you would like to help test early versions of OSCAR, please see the Wiki page about testing OSCAR.  We welcome everyone who would like to test OSCAR, help develop OSCAR, and help with translations to existing or new languages. https://www.sleepfiles.com/OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2562"/>
        <source>Line Chart</source>
        <translation>線形图</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="540"/>
        <source>dd MMMM yyyy</source>
        <translation>天天 月月月月 年年年年</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1448"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;真极大值是資料設定的最大值.&lt;/p&gt;&lt;p&gt;滤除百分之九十九的异常值.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2302"/>
        <location filename="../oscar/preferencesdialog.ui" line="2306"/>
        <source>Profile</source>
        <translation>個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="464"/>
        <source>Flag Type</source>
        <translation>標記類型</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="815"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>計算非意識漏氣量</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="623"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>在啟動時自動載入上次使用的個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2491"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>設定工具提示可见時間长度。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="528"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>双击變更 &apos;%1通道的描述資訊.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="190"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>缩小多個療程間距可以使其顯示在同一天.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1213"/>
        <source>Are you really sure you want to do this?</source>
        <translation>確定進行此操作?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1125"/>
        <source>Duration of airflow restriction</source>
        <translation>氣流限制的持续時間</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2557"/>
        <source>Bar Tops</source>
        <translation>任務条置顶</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="808"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>這一計算需要PAP记录的總漏氣量)

非意識漏氣量的計算是線性的，因為没有面罩排氣曲線可参考.

如果你佩戴不同的面罩，請選取平均值，值应足够接近.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="468"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>這使得OSCAR的資料占用了大约一半的空間。
但它使匯入和日期變化需要更长的時間..
建議使用带有小型固态硬盘的計算。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="97"/>
        <source>Session Splitting Settings</source>
        <translation>療程拆分設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="738"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;注意: 不能够進行時区自動矫正，請确保您操作系统時間以及時区設定正确.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2671"/>
        <source>Other Visual Settings</source>
        <translation>其他顯示設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="316"/>
        <source>Day Split Time</source>
        <translation>時段</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="615"/>
        <source>CPAP Waveforms</source>
        <translation>CPAP波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="473"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>压缩療程資料（使OSCAR資料量变小，但日期變化较慢。）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3076"/>
        <source>Big  Text</source>
        <translation>大字体</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2711"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;此项功能已被取消，但會在后续版本内加入. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="953"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>注意:預設选用線性計算法。如果變更資料需重新計算.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1165"/>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>ResMed S9设备會定期從SD卡内删除7天和30天以内的資料（取决於分辨率）。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1306"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>注意使用時間低於4個小時的日期。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="507"/>
        <source>Do not import sessions older than:</source>
        <translation>請不要匯入早於如下日期的療程:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2721"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>點擊日常查看导航按钮將跳过没有資料记录的日期</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1648"/>
        <source>Flag Pulse Rate Above</source>
        <translation>心率標記高</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1638"/>
        <source>Flag Pulse Rate Below</source>
        <translation>心率標記低</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="792"/>
        <source>Seconds</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1229"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>為AHI/小時圖表的每一個点调节資料量
預設值到60分鐘，建議使用這一預設值。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1118"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>在事件分类饼图中顯示</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1578"/>
        <source>Other oximetry options</source>
        <translation>其他血氧儀選項</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2334"/>
        <source>Switch Tabs</source>
        <translation>切换标签</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="480"/>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>保留ResMed设备SD卡資料的備份，
ResMed S9系列设备删除超过7天的高分辨率資料，
以及超过30天的圖表資料。

如果您需要重新安装，OSCAR可以保留此資料的副本。
（强烈推荐，除非你的磁盘空間不足或者不关心圖形資料）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3174"/>
        <source>&amp;Cancel</source>
        <translation>&amp;取消</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="365"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>不可分割（警告：請阅读工工具提示資訊）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2173"/>
        <source>Last Checked For Updates: </source>
        <translation>上次的更新: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="438"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>压缩Rresmed（EDF）備份以节省磁盘空間。
備份的EDF檔案以.gz格式儲存，
這在Mac和Mac上很常见 Linux平台..

OSCAR可以本地從此压缩備份目录匯入..
要將其與ResScan一起使用，首先需要解压缩.gz檔案。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3142"/>
        <location filename="../oscar/preferencesdialog.cpp" line="466"/>
        <location filename="../oscar/preferencesdialog.cpp" line="598"/>
        <source>Details</source>
        <translation>詳細資料</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2684"/>
        <source>Use Anti-Aliasing</source>
        <translation>使用圖形保真技术顯示</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2714"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>動画 &amp;&amp; 爱好</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="70"/>
        <source>&amp;Import</source>
        <translation>&amp;匯入</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2326"/>
        <location filename="../oscar/preferencesdialog.ui" line="2365"/>
        <source>Statistics</source>
        <translation>統計值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="603"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;這一設定將波形以及事件資料保存在内存中以便於提升再次存取的速度.&lt;/p&gt;&lt;p&gt;這不是一项必須開啟的設定，因為操作系统會缓存載入过的資料.&lt;/p&gt;&lt;p&gt;建議保持這一設定呈關閉状态，除非内存非常大.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1354"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>變更如下設定需要重启，但不需要重新估算。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2254"/>
        <source>&amp;Appearance</source>
        <translation>&amp;外观</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2445"/>
        <source>The pixel thickness of line plots</source>
        <translation>線条图的像素厚度</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="541"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>此标志是否有专用的概览圖表.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="562"/>
        <location filename="../oscar/preferencesdialog.cpp" line="695"/>
        <source>This is a description of what this channel does.</source>
        <translation>此顯示的是這個通道的作用.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="160"/>
        <source>Combine Close Sessions </source>
        <translation>關閉所有療程 </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="977"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>自訂PAP使用者事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1407"/>
        <source>Cumulative Indices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1788"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1874"/>
        <location filename="../oscar/preferencesdialog.ui" line="1953"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;警告: &lt;/span&gt;這仅仅是提示您可以這么做，但這不是個好建議.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2211"/>
        <source>I want to be notified of test versions. (Advanced users only please.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2734"/>
        <source>Allow YAxis Scaling</source>
        <translation>允许Y轴缩放</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2741"/>
        <source>Whether to include machine serial number on machine settings changes report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2744"/>
        <source>Include Serial Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2751"/>
        <source>Print reports in black and white, which can be more legible on non-color printers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2754"/>
        <source>Print reports in black and white (monochrome)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2786"/>
        <source>Fonts (Application wide settings)</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2704"/>
        <source>Use Pixmap Caching</source>
        <translation>使用像素映射缓存</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2120"/>
        <source>Check for new version every</source>
        <translation>检查是否有新版本</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1887"/>
        <source>Waveforms</source>
        <translation>波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1393"/>
        <source>Maximum Calcs</source>
        <translation>最大估算值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2321"/>
        <location filename="../oscar/preferencesdialog.ui" line="2360"/>
        <location filename="../oscar/preferencesdialog.cpp" line="463"/>
        <location filename="../oscar/preferencesdialog.cpp" line="594"/>
        <source>Overview</source>
        <translation>總覽</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2469"/>
        <source>Tooltip Timeout</source>
        <translation>工具提示超時</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="29"/>
        <source>Preferences</source>
        <translation>参数設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source>General CPAP and Related Settings</source>
        <translation>通用PAP以及相关設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2576"/>
        <source>Default display height of graphs in pixels</source>
        <translation>使用預設项目顯示图标高度</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2399"/>
        <source>Overlay Flags</source>
        <translation>叠加標記</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2051"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>如果OSCAR出现圖形渲染问题，請嘗試從預設設定（桌面OpenGL）變更此設定。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2691"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>在特定區塊顯示更多的方波。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2311"/>
        <location filename="../oscar/preferencesdialog.ui" line="2350"/>
        <source>Welcome</source>
        <translation>欢迎使用</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1741"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>血氧飽和百分比下降</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1966"/>
        <source>&amp;General</source>
        <translation>&amp;通用</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1476"/>
        <source>Standard average of indice</source>
        <translation>標準平均值</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1167"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> 如果需要节省磁盘空間，請手動備份。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="446"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>压缩備份SD卡資料（节省空間但匯入速度变慢）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="606"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>保持波形/事件資料在内存中</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="674"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>是否顯示此波形的细分概览。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1428"/>
        <source>Normal Average</source>
        <translation>正常体重</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="617"/>
        <source>Positional Waveforms</source>
        <translation>位置波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="804"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>為了變更資料設定將重建索引，這將花费几分鐘的時間。

確定要變更資料吗?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="484"/>
        <source>Positional Events</source>
        <translation>位置事件</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1357"/>
        <source>Preferred Calculation Methods</source>
        <translation>首選計算方法</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1466"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>合並计数除以總小時数</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2549"/>
        <source>Graph Tooltips</source>
        <translation>圖形工具提示</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1527"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;血氧测量</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="711"/>
        <source>CPAP Clock Drift</source>
        <translation>CPAP時鐘漂移</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="679"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>在此可以為%1波形設定&lt;b&gt;更低的&lt;/b&gt; 閥值来進行某些計算</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="583"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>開啟個人檔案后自動啟動CPAP匯入程序</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2694"/>
        <source>Square Wave Plots</source>
        <translation>方波图</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2186"/>
        <source>TextLabel</source>
        <translation>文本标签</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1264"/>
        <source>Preferred major event index</source>
        <translation>首選主要事件索引</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2890"/>
        <source>Application</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2409"/>
        <source>Line Thickness</source>
        <translation>線宽</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.cpp" line="95"/>
        <source>Name</source>
        <translation>姓名</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>Bytes</source>
        <translation>位元</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>Sorry</source>
        <translation>歹勢</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="515"/>
        <source>Profile: %1</source>
        <translation>個人檔案: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>請注意：這將不可避免地删除個人檔案以及儲存在%2下的所有備份資料。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="135"/>
        <location filename="../oscar/profileselector.cpp" line="312"/>
        <source>%1, %2</source>
        <translation>%1% %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="170"/>
        <source>You must create a profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="357"/>
        <source>The selected profile does not appear to contain any data and cannot be removed by OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>KB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>GB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>TB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>PB</source>
        <translation type="unfinished">周期性呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="506"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>電郵: &lt;a href=‘mailto:%1’&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="435"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>個人檔案 &apos;%1&apos;已成功删除</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="465"/>
        <source>Summaries:</source>
        <translation>摘要：</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="417"/>
        <source>DELETE</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Forgot your password?</source>
        <translation>忘记密碼？</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="240"/>
        <source>&amp;New Profile</source>
        <translation>&amp;新建個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="431"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>個人檔案目录出错,請手動移除.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="482"/>
        <source>Show disk usage information</source>
        <translation>顯示磁盘使用資訊</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="503"/>
        <source>Phone: %1</source>
        <translation>电话号码:%1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="233"/>
        <location filename="../oscar/profileselector.cpp" line="367"/>
        <source>Enter Password for %1</source>
        <translation>輸入密碼 %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Last Imported</source>
        <translation>最新匯入</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Profile</source>
        <translation>個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="467"/>
        <source>Backups:</source>
        <translation>備份：</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="500"/>
        <source>Name: %1, %2</source>
        <translation>名字: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="278"/>
        <source>Please select or create a profile...</source>
        <translation>請選取或創建一個個人檔案...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="249"/>
        <location filename="../oscar/profileselector.cpp" line="386"/>
        <source>You entered an incorrect password</source>
        <translation>密碼不正确</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="479"/>
        <location filename="../oscar/profileselector.cpp" line="519"/>
        <source>Hide disk usage information</source>
        <translation>隐藏磁盘使用資訊</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="512"/>
        <source>No profile information given</source>
        <translation>未提供個人檔案資訊</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="509"/>
        <source>Address:</source>
        <translation>地址:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>在论坛上询问如何重新設定。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="322"/>
        <source>Select a profile first</source>
        <translation>首先選取個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Other Data</source>
        <translation>其他参数</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="198"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="466"/>
        <source>Events:</source>
        <translation>事件：</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="215"/>
        <source>&amp;Open Profile</source>
        <translation>&amp;開啟個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>筛选：</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="328"/>
        <source>Destroy Profile</source>
        <translation>删除個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="226"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;編輯個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="389"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>如果由於忘记密碼而试图删除，则需要重新設定密碼或手動删除個人檔案資料夾。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>如下图所示，請確認输入資訊：DELETE。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Brand</source>
        <translation>PAP品牌</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="258"/>
        <source>Profile: None</source>
        <translation>個人檔案：無</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Ventilator Model</source>
        <translation>PAP型号</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>你將要删除個人檔案&apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>需要输入大写字母 DELETE.</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="57"/>
        <source>Abort</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="752"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="167"/>
        <source>A</source>
        <translation>未分类</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="750"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>H</source>
        <translation>低通氣</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="764"/>
        <source>P</source>
        <translation>壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>h</source>
        <translation>小時</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="192"/>
        <source>Built with Qt %1 on %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="194"/>
        <source>Operating system:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="195"/>
        <source>Graphics Engine:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="196"/>
        <source>Graphics Engine type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="202"/>
        <source>App key:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>ANGLE / OpenGLES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source> m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source> cm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>m</source>
        <translation>分鐘</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <source>s</source>
        <translation>秒s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="681"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <source>Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="313"/>
        <source>AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="770"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>UF1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>UF2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>UF3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <source>AI</source>
        <translation>呼吸中止</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="754"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>CA</source>
        <translation>中枢性</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="188"/>
        <source>EP</source>
        <translation>呼氣壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>FL</source>
        <translation>氣流受限</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <source>HI</source>
        <translation>低通氣指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>CSR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <source>IE</source>
        <translation>呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="757"/>
        <source>LE</source>
        <translation>漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>LF</source>
        <translation>漏氣标志</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="182"/>
        <source>LL</source>
        <translation>大量漏氣</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>Kg</source>
        <translation>公斤</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="768"/>
        <source>O2</source>
        <translation>氧氣</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>OA</source>
        <translation>阻塞性</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>NR</source>
        <translation>未影響事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>PB</source>
        <translation>周期性呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2799"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>PC</source>
        <translation>混合面罩</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="857"/>
        <source>No</source>
        <translation>不</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="763"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2782"/>
        <source>PP</source>
        <translation>最高壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="701"/>
        <source>Only Settings and Compliance Data Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>Summary Data Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <source>PS</source>
        <translation>壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <location filename="../oscar/SleepLib/journal.cpp" line="26"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="832"/>
        <source>Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="853"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="178"/>
        <source>On</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>RE</source>
        <translation>呼吸作用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="191"/>
        <source>SA</source>
        <translation>呼吸中止</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="753"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="165"/>
        <source>UA</source>
        <translation>未知中止</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="173"/>
        <source>VS</source>
        <translation>打鼾指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="668"/>
        <source>ft</source>
        <translation>英尺</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>lb</source>
        <translation>磅</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>ml</source>
        <translation>毫升</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <source>ms</source>
        <translation>毫秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <source>oz</source>
        <translation>盎司</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="709"/>
        <source>&amp;No</source>
        <translation>&amp;不</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>AHI</source>
        <translation>呼吸中止指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="744"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2796"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="110"/>
        <source>ASV</source>
        <translation>适应性支持通氣模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>BMI</source>
        <translation>体重指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <source>CAI</source>
        <translation>中枢性中止指數</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Apr</source>
        <translation>四月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Aug</source>
        <translation>八月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="213"/>
        <location filename="../oscar/SleepLib/common.cpp" line="866"/>
        <source>W-Avg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="215"/>
        <location filename="../oscar/SleepLib/common.cpp" line="865"/>
        <source>Avg</source>
        <translation>平均</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="279"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="283"/>
        <source>%1: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="287"/>
        <source>???: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="361"/>
        <source>% in %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1245"/>
        <source>%1 %2 / %3 / %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>DOB</source>
        <translation>生日</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <source>EPI</source>
        <translation>呼氣壓力指數</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Dec</source>
        <translation>十二月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="781"/>
        <source>FLI</source>
        <translation>氣流受限指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="852"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>End</source>
        <translation>結束</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Feb</source>
        <translation>二月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jan</source>
        <translation>一月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Jul</source>
        <translation>七月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jun</source>
        <translation>六月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="767"/>
        <source>NRI</source>
        <translation>未影響事件指數</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Mar</source>
        <translation>三月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="860"/>
        <source>Max</source>
        <translation>最大</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>May</source>
        <translation>五月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="861"/>
        <source>Med</source>
        <translation>中間值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="859"/>
        <source>Min</source>
        <translation>最小</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Nov</source>
        <translation>十一月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Oct</source>
        <translation>十月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="854"/>
        <source>Off</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="776"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="284"/>
        <source>RDI</source>
        <translation>呼吸紊乱指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="783"/>
        <source>REI</source>
        <translation>呼吸作用指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <source>UAI</source>
        <translation>未知中止指數</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Sep</source>
        <translation>九月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>VS2</source>
        <translation>打鼾指數2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="856"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <source>bpm</source>
        <translation>次每分鐘</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>Brain Wave</source>
        <translation>脑波</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="708"/>
        <source>&amp;Yes</source>
        <translation>&amp;是</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="104"/>
        <source>APAP</source>
        <translation>全自動正压通氣</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2791"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="103"/>
        <source>CPAP</source>
        <translation>持续氣道正压通氣</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="855"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2895"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2986"/>
        <source>Auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="697"/>
        <source>Busy</source>
        <translation>忙</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="738"/>
        <source>Min EPAP</source>
        <translation>呼氣壓力最小值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <source>EPAP</source>
        <translation>呼氣壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="840"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="741"/>
        <source>Min IPAP</source>
        <translation>吸氣壓力最小值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <source>IPAP</source>
        <translation>吸氣壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="850"/>
        <source>Last</source>
        <translation>最近一次</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="804"/>
        <source>Leak</source>
        <translation>漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="187"/>
        <source>Mask</source>
        <translation>面罩</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="211"/>
        <source>Med.</source>
        <translation>中間值.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2786"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2788"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="99"/>
        <source>Mode</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="834"/>
        <source>Name</source>
        <translation>姓名</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="846"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="762"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>RERA</source>
        <translation>呼吸努力相关性觉醒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="195"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1039"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Ramp</source>
        <translation>斜坡啟動</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="209"/>
        <source>SpO2</source>
        <translation>血氧飽和度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="775"/>
        <source>Zero</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="263"/>
        <source>Resp. Event</source>
        <translation>呼吸時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="291"/>
        <source>Inclination</source>
        <translation>侧卧</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="482"/>
        <source>Launching Windows Explorer failed</source>
        <translation>啟動视窗浏览器失败</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="535"/>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="549"/>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="556"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="591"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>是否要開啟自動備份，新版的OSCAR可以從這些版本重建？</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2907"/>
        <source>Hose Diameter</source>
        <translation>管径</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="712"/>
        <source>&amp;Save</source>
        <translation>&amp;保存</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="176"/>
        <location filename="../oscar/SleepLib/day.cpp" line="178"/>
        <location filename="../oscar/SleepLib/day.cpp" line="180"/>
        <location filename="../oscar/SleepLib/day.cpp" line="185"/>
        <source>%1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="185"/>
        <source>99.5%</source>
        <translation>90% {99.5%?}</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="189"/>
        <source>%1% %2</source>
        <translation type="unfinished">%1% %2m {1%?} {2?}</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1534"/>
        <source>varies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1546"/>
        <source>%1%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1568"/>
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1606"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1627"/>
        <source>EPAP %1-%2 IPAP %3-%4 (%5)</source>
        <translation type="unfinished">呼氣壓力 %1 吸氣壓力 %2 %3 (%4) {1-%2 ?} {3-%4 ?} {5)?}</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Therapy Pressure</source>
        <translation>治療壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="735"/>
        <source>BiPAP</source>
        <translation>双水平氣道正压通氣</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="823"/>
        <source>Brand</source>
        <translation>品牌</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="92"/>
        <source>ResMed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="93"/>
        <source>S9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="126"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="98"/>
        <source>EPR: </source>
        <translation>呼氣壓力释放: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>Daily</source>
        <translation>日常</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <source>Email</source>
        <translation>电子邮件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="694"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2870"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="849"/>
        <source>First</source>
        <translation>第一次</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Ramp Pressure</source>
        <translation>壓力上升</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="684"/>
        <source>L/min</source>
        <translation>升/分鐘</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="367"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="694"/>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>Hours</source>
        <translation>小時</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <source>Leaks</source>
        <translation>漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="265"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="275"/>
        <source>Max: </source>
        <translation>最大: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="260"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="270"/>
        <source>Min: </source>
        <translation>最小: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <source>Model</source>
        <translation>型式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="191"/>
        <source>Nasal</source>
        <translation>鼻罩</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Notes</source>
        <translation>備註</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="836"/>
        <source>Phone</source>
        <translation>电话号码</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="847"/>
        <source>Ready</source>
        <translation>就緒</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1129"/>
        <source>TTIA:</source>
        <translation>呼吸中止總時間:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="803"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2804"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Snore</source>
        <translation>打鼾</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="851"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <source>Usage</source>
        <translation>使用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="284"/>
        <source>Respiratory Disturbance Index</source>
        <translation>呼吸紊乱指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <source>cmH2O</source>
        <translation>厘米水柱</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="141"/>
        <source>Pressure Support</source>
        <translation>壓力支持</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Bedtime: %1</source>
        <translation>睡眠時間:%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1170"/>
        <source>90%</source>
        <translation type="unfinished">99.5% {90%?}</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>Hypopnea</source>
        <translation>低通氣</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>ratio</source>
        <translation>比率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>Tidal Volume</source>
        <translation>呼吸容量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2694"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="827"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="225"/>
        <source>Getting Ready...</source>
        <translation>準備就緒...</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="294"/>
        <source>AI=%1 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="439"/>
        <source>Entire Day</source>
        <translation>整天</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="555"/>
        <source>%1 %2 %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2787"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>Intellipa壓力释放模式.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Personal Sleep Coach</source>
        <translation>個人睡眠教练</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Zeo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="359"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>最新血氧测定資料：&lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="361"/>
        <source>(1 day ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="362"/>
        <source>(%2 days ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="705"/>
        <source>Scanning Files</source>
        <translation>正在扫描檔案</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Clear Airway</source>
        <translation>开放氣道</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="204"/>
        <source>Heart rate in beats per minute</source>
        <translation>心臟每分鐘的跳動次数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="182"/>
        <source>A large mask leak affecting machine performance.</source>
        <translation>大量漏氣影响PAP性能.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time spent awake</source>
        <translation>清醒時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="162"/>
        <source>Temp. Enable</source>
        <translation>温度啟用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3037"/>
        <source>Timed Breath</source>
        <translation>短時間的呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="390"/>
        <source>Pop out Graph</source>
        <translation>弹出圖表</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="473"/>
        <source>The popout window is full. You should capture the existing
popout window, delete it, then pop out this graph again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1444"/>
        <source>Your machine doesn&apos;t record data to graph in Daily View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1593"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2151"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2194"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2265"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2282"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>Using </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>, found SleepyHead -
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="203"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>必須執行OSCAR移轉工具</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="297"/>
        <source>Mask On Time</source>
        <translation>面具使用時間</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="192"/>
        <source>Choose the SleepyHead or OSCAR data folder to migrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="193"/>
        <source>or CANCEL to skip migration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="207"/>
        <source>The folder you chose does not contain valid SleepyHead or OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="208"/>
        <source>You cannot use this folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="223"/>
        <source>Migrating </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="223"/>
        <source> files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source>from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source>to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="331"/>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="332"/>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="499"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="500"/>
        <source>If you have been using SleepyHead or an older version of OSCAR,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="501"/>
        <source>OSCAR can copy your old data to this folder later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="502"/>
        <source>We suggest you use this folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="503"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="509"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="514"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="546"/>
        <source>Data directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="554"/>
        <source>Migrate SleepyHead or OSCAR Data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="555"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead or OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="556"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead or OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="565"/>
        <source>Unable to create the OSCAR data folder at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="575"/>
        <source>Unable to write to OSCAR data directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="576"/>
        <source>Error code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="577"/>
        <source>OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="588"/>
        <source>Unable to write to debug log. You can still use the debug pane (Help/Troubleshooting/Show Debug Pane) but the debug log will not be written to disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="647"/>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="656"/>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="660"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>此操作會损坏資料，是否繼續?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="467"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="482"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>正在載入個人檔案&quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="948"/>
        <source>Chromebook file system detected, but no removable device found
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="949"/>
        <source>You must share your SD card with Linux using the ChromeOS Files program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2112"/>
        <source>Recompressing Session Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2633"/>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2680"/>
        <location filename="../oscar/mainwindow.cpp" line="2730"/>
        <location filename="../oscar/mainwindow.cpp" line="2789"/>
        <source>Unable to create zip!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3030"/>
        <source>Breathing Not Detected</source>
        <translation>呼吸未被檢測到</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1447"/>
        <source>There is no data to graph</source>
        <translation>没有資料可供圖表</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Journal</source>
        <translation>日誌</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="533"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>正在查找str.edf檔案...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <source>Pat. Trig. Breath</source>
        <translation>患者触发呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1242"/>
        <source>(Summary Only)</source>
        <translation>（摘要）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Ramp Delay Period</source>
        <translation>斜坡升压期間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>Sessions Switched Off</source>
        <translation>關閉療程</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Awakenings</source>
        <translation>觉醒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="574"/>
        <source>This folder currently resides at the following location:</source>
        <translation>本地檔案位置:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Morning Feel</source>
        <translation>晨起感觉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>Pulse Change</source>
        <translation>脈搏變化</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2857"/>
        <source>Disconnected</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Sleep Stage</source>
        <translation>睡眠階段</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Minute Vent.</source>
        <translation>分鐘通氣率.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>SpO2 Drop</source>
        <translation>血氧飽和度降低</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Ramp Event</source>
        <translation>斜坡啟動事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="191"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>觉醒偵測功能會在偵測到醒来時降低PAP的壓力.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2247"/>
        <source>Show All Events</source>
        <translation>顯示所有事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="291"/>
        <source>Upright angle in degrees</source>
        <translation>垂直</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="860"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="729"/>
        <location filename="../oscar/mainwindow.cpp" line="2311"/>
        <source>Importing Sessions...</source>
        <translation>匯入療程...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="112"/>
        <source>%1: %2</source>
        <translation>%1% %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>Higher Expiratory Pressure</source>
        <translation>更高的呼氣壓力</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="291"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>未影響事件指數=%1 漏氣指數=%2 呼氣壓力指數=%3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="557"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>請单击“否”退出，並在再次啟動OSCAR之前手動備份您的個人檔案。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="173"/>
        <source>A vibratory snore</source>
        <translation>一次振動打鼾</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="173"/>
        <source>Vibratory Snore</source>
        <translation>振動打鼾</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="513"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>由於没有選取資料資料夾，OSCAR將退出。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>更低的吸氣壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="139"/>
        <source>Humidifier Enabled Status</source>
        <translation>湿化器已啟用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="175"/>
        <source>Essentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="177"/>
        <source>Plus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="190"/>
        <source>Full Face</source>
        <translation>全臉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2717"/>
        <source>Backing up files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2724"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="249"/>
        <source>Reading data files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2794"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="122"/>
        <source>Full Time</source>
        <translation>全部時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2797"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2799"/>
        <source>SmartFlex Level</source>
        <translation>SmartFlex 级别</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2805"/>
        <source>Snoring event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2806"/>
        <source>SN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="26"/>
        <source>Journal Data</source>
        <translation>日誌</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1003"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% 依從性, 定义為 &gt; %2 小時)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Resp. Rate</source>
        <translation>呼吸速率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="791"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="260"/>
        <source>Insp. Time</source>
        <translation>吸氣時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="257"/>
        <source>Exp. Time</source>
        <translation>呼氣時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="608"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>Oscar现在將退出，然后（嘗試）啟動計算机檔案管理器，以便手動備份個人檔案：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="158"/>
        <source>ClimateLine Temperature</source>
        <translation>加热管路温度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="151"/>
        <source>Your %1 %2 (%3) generated data that OSCAR has never seen before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="152"/>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this machine&apos;s SD card and matching clinician .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="162"/>
        <source>Your %1 CPAP machine (Model %2) is unfortunately not a data capable model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="176"/>
        <source>Your %1 CPAP machine (Model %2) has not been tested yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="177"/>
        <source>It seems similar enough to other machines that it might work, but the developers would like a .zip copy of this machine&apos;s SD card and matching clinician .pdf reports to make sure it works with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="185"/>
        <source>Machine Unsupported</source>
        <translation>不支持的机型</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="186"/>
        <source>Sorry, your %1 CPAP machine (%2) is not supported yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="187"/>
        <source>The developers need a .zip copy of this machine&apos;s SD card and matching clinician .pdf reports to make it work with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>脈搏的强度的相关评估</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="826"/>
        <source>Machine</source>
        <translation>機器</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="947"/>
        <source>Mask On</source>
        <translation>面罩开启</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="294"/>
        <source>Max: %1</source>
        <translation>最大：%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="572"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>歹勢，清除操作失败，此版本的OSCAR無法啟動。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>血氧飽和度突然降低</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Time spent in deep sleep</source>
        <translation>深層睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="40"/>
        <source>There are no graphs visible to print</source>
        <translation>無可列印圖表</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="55"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>OSCAR將會使用其中的第一個：

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Target Vent.</source>
        <translation>目標通氣率.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="288"/>
        <source>Sleep position in degrees</source>
        <translation>睡眠体位角度</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="825"/>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1050"/>
        <source>Plots Disabled</source>
        <translation>停用區塊</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="229"/>
        <source>Min: %1</source>
        <translation>最小:%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>Minutes</source>
        <translation>分鐘</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>Periodic Breathing</source>
        <translation>周期性呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2590"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2665"/>
        <source>Popout %1 Graph</source>
        <translation>弹出圖表%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2793"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="121"/>
        <source>Ramp Only</source>
        <translation>仅斜坡升压</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Ramp Time</source>
        <translation>斜坡升压時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="54"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation>出於某種原因，Oscar在您的個人檔案中找不到日誌对象记录，但找到了多個日誌資料資料夾。

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2805"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>PRS1 壓力释放模式.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>周期性呼吸的不正常時期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="187"/>
        <source>ResMed Mask Setting</source>
        <translation>ResMed面罩設定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>瑞思迈呼氣壓力释放</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="112"/>
        <source>iVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="114"/>
        <source>Auto for Her</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1028"/>
        <source>EPR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="126"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1034"/>
        <source>EPR Level</source>
        <translation>呼氣壓力释放水平</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="202"/>
        <source>Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="205"/>
        <source>Soft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="208"/>
        <source>SmartStop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="208"/>
        <source>Machine auto stops by breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="208"/>
        <source>Smart Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="214"/>
        <source>Patient View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="217"/>
        <source>Simple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="216"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="105"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="108"/>
        <source>BiPAP-T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="106"/>
        <source>BiPAP-S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="107"/>
        <source>BiPAP-S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="113"/>
        <source>PAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="441"/>
        <source>Your ResMed CPAP machine (Model %1) has not been tested yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="442"/>
        <source>It seems similar enough to other machines that it might work, but the developers would like a .zip copy of this machine&apos;s SD card to make sure it works with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1209"/>
        <source>Parsing STR.edf records...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="809"/>
        <source>Unintentional Leaks</source>
        <translation>無意識漏氣量</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="55"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>是否希望在報告中顯示標記区域?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="109"/>
        <source>VPAPauto</source>
        <translation>VPAP全自動</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Apnea Hypopnea Index</source>
        <translation>呼吸中止指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Physical Height</source>
        <translation>身高</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="175"/>
        <source>Pt. Access</source>
        <translation>患者通道</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV模式 (固定呼氣壓力)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Patient Triggered Breaths</source>
        <translation>患者出发的呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="553"/>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation>這意味着您需要自行由您的記錄或者資料卡中匯入資料.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="379"/>
        <source>Events</source>
        <translation>事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2884"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="145"/>
        <source>Humid. Level</source>
        <translation>湿度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="169"/>
        <source>AB Filter</source>
        <translation>抗菌過濾棉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Height</source>
        <translation>身高</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="195"/>
        <source>Ramp Enable</source>
        <translation>斜坡升压啟動</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="394"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 事件)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="781"/>
        <source>Lower Threshold</source>
        <translation>降低</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="836"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1248"/>
        <source>No Data</source>
        <translation>無資料</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="345"/>
        <source>Zeo sleep quality measurement</source>
        <translation>ZEO睡眠质量监测</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="564"/>
        <source>Page %1 of %2</source>
        <translation>页码 %1 到 %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>Litres</source>
        <translation>升</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="184"/>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="864"/>
        <source>Median</source>
        <translation>中值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1585"/>
        <source>Fixed %1 (%2)</source>
        <translation>固定 %1 (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="373"/>
        <source>Min %1</source>
        <translation>最小 %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="483"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>未找到视窗浏览器的可执行檔案.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2945"/>
        <source>Machine automatically switches off</source>
        <translation>PAP自動關閉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2858"/>
        <source>Connected</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>Low Usage Days: %1</source>
        <translation>低使用天数:%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>PS Max</source>
        <translation>壓力支持最大壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>PS Min</source>
        <translation>最小壓力</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="203"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>資料库过期
請重建PAP資料</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="266"/>
        <source>Flow Limit.</source>
        <translation>氣流限制.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>包含自然漏氣在内的面罩漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="719"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Plethy</source>
        <translation>足够的</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1022"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="191"/>
        <source>SensAwake</source>
        <translation>觉醒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="746"/>
        <source>ST/ASV</source>
        <translation>自发/定時 ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="281"/>
        <source>Median Leaks</source>
        <translation>漏氣率中值</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="133"/>
        <source>%1 Report</source>
        <translation>%1報告</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <source>Pr. Relief</source>
        <translation>壓力释放</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="702"/>
        <source>Graphs Switched Off</source>
        <translation>關閉圖表</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <source>Serial</source>
        <translation>串号</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <source>Series</source>
        <translation>系列</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="209"/>
        <source>SpO2 %</source>
        <translation>血氧飽和度 %</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="360"/>
        <source>(last night)</source>
        <translation>(昨晚）</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="246"/>
        <source>AHI	%1
</source>
        <translation>呼吸中止指數（AHI）%1
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Weight</source>
        <translation>体重</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="345"/>
        <source>ZEO ZQ</source>
        <translation>ZEP睡商</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2821"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>PRS1 壓力释放設定.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="288"/>
        <source>Orientation</source>
        <translation>定位</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="133"/>
        <source>Smart Start</source>
        <translation>自啟動</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="728"/>
        <source>Event Flags</source>
        <translation>呼吸事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2936"/>
        <source>A few breaths automatically starts machine</source>
        <translation>自動開啟機器在几次呼吸后</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="345"/>
        <source>Zeo ZQ</source>
        <translation>ZEO 睡商</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="739"/>
        <source>Migrating Summary File Location</source>
        <translation>正在移轉摘要檔案位置</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="716"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Zombie</source>
        <translation>呆瓜</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <source>Bookmarks</source>
        <translation>標記簇</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2787"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>PAP Mode</source>
        <translation>正压通氣模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="99"/>
        <source>CPAP Mode</source>
        <translation>CPAP模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Time taken to get to sleep</source>
        <translation>入睡時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>DeVilbiss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>Intellipap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="86"/>
        <source>SmartFlex Settings</source>
        <translation>SmartFlex設定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>An apnea where the airway is open</source>
        <translation>氣道开放情况下的呼吸中止</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="794"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="266"/>
        <source>Flow Limitation</source>
        <translation>氣流受限</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2667"/>
        <source>Pin %1 Graph</source>
        <translation>標示%1圖表</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2588"/>
        <source>Unpin %1 Graph</source>
        <translation>解除標示%1圖表</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="713"/>
        <source>Queueing Import Tasks...</source>
        <translation>正在排队匯入任務...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="161"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>小時数:%1小時.%2分鐘,%3秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="552"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>OSCAR尚未為此设备儲存任何備份。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="958"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
长度: %3
开始: %2</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="244"/>
        <source>RDI	%1
</source>
        <translation>呼吸紊乱指數(RDI)	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="111"/>
        <source>ASVAuto</source>
        <translation>ASV全自動</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1596"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>壓力 %1 超过 %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="226"/>
        <source>Flow Rate</source>
        <translation>氣流速率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="257"/>
        <source>Time taken to breathe out</source>
        <translation>呼氣時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="556"/>
        <source>Important:</source>
        <translation>重要提示:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="133"/>
        <source>Machine auto starts by breathing</source>
        <translation>呼吸触发啟動</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>光学探测顯示心率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="694"/>
        <source>Loading %1 data for %2...</source>
        <translation>正在為%2載入%1資料...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="189"/>
        <source>Pillows</source>
        <translation>鼻枕</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="945"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
长度: %3
开始: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time Awake</source>
        <translation>清醒時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>How you felt in the morning</source>
        <translation>早上醒来的感觉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="254"/>
        <source>I:E Ratio</source>
        <translation>呼吸比率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>Amount of air displaced per breath</source>
        <translation>每次呼吸氣量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Pat. Trig. Breaths</source>
        <translation>患者触发呼吸率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="145"/>
        <source>Humidity Level</source>
        <translation>湿度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="723"/>
        <source>Profile</source>
        <translation>個人檔案</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="837"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>Leak Flag</source>
        <translation>漏氣标志</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Leak Rate</source>
        <translation>漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="909"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>載入摘要.xml.gz檔案</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="162"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>加热管路温度啟用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>Severity (0-1)</source>
        <translation>嚴重程度 (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="362"/>
        <source>Reporting from %1 to %2</source>
        <translation>正在生成由 %1 到 %2 的報告</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1201"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>確定將所有通道颜色恢复預設設定吗?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>BrainWave</source>
        <translation>脑波</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Inspiratory Pressure</source>
        <translation>吸氣壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2954"/>
        <source>Whether or not machine allows Mask checking.</source>
        <translation>是否允许PAP進行面罩检查.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Number of Awakenings</source>
        <translation>觉醒次数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2781"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>通过壓力脉冲&apos;砰&apos;可以偵測到氣道關閉.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2798"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Intellipap 壓力释放水平.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>Non Responding Event</source>
        <translation>未回應事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="281"/>
        <source>Median Leak Rate</source>
        <translation>漏氣率中值</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="387"/>
        <source> (%3 sec)</source>
        <translation> (%3 秒)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Rate of breaths per minute</source>
        <translation>每分鐘呼吸的次数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="558"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>準備升级，是否執行新版本的OSCAR？</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="168"/>
        <source>Updating Statistics cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="715"/>
        <source>Usage Statistics</source>
        <translation>使用統計值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Perfusion Index</source>
        <translation>灌注指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Graph displaying snore volume</source>
        <translation>圖形顯示打鼾指數</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="947"/>
        <source>Mask Off</source>
        <translation>面罩關閉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <source>Max EPAP</source>
        <translation>呼氣壓力最大值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="742"/>
        <source>Max IPAP</source>
        <translation>吸氣壓力最大值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="842"/>
        <source>Bedtime</source>
        <translation>睡眠時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1592"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>呼氣壓力 %1 吸氣壓力%2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <source>Pressure</source>
        <translation>壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2935"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2937"/>
        <source>Auto On</source>
        <translation>自動開啟</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="863"/>
        <source>Average</source>
        <translation>平均</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Target Minute Ventilation</source>
        <translation>目標分鐘通氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Amount of air displaced per minute</source>
        <translation>每分鐘的换氣量</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1144"/>
        <source>
TTIA: %1</source>
        <translation>
呼吸中止總時間: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>患者出发的呼吸百分比</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="161"/>
        <source>Non Data Capable Machine</source>
        <translation>没有使用機器的資料</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="998"/>
        <source>Days: %1</source>
        <translation>天数:%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Plethysomogram</source>
        <translation>体积描述术</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="165"/>
        <source>Unclassified Apnea</source>
        <translation>未分類的呼吸中止</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>由OSCAR的流量波形處理器檢測到的使用者自訂事件。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source>Software Engine</source>
        <translation>應用程式引擎</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>自動双水平</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>Please Note</source>
        <translation>請留言</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Starting Ramp Pressure</source>
        <translation>开始斜坡升压</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Last Updated</source>
        <translation>最近更新</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="188"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Intellipap偵測到的嘴部呼吸事件.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV模式 (可变呼氣壓力)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="126"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>呼氣壓力释放水平</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="795"/>
        <source>Flow Limit</source>
        <translation>氣流受限</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="289"/>
        <source>UAI=%1 </source>
        <translation>未知中止指數=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="745"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 很少使用, %2 不使用, 超过 %3 天 (%4% 兼容.) 长度: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1041"/>
        <source>Loading Summary Data</source>
        <translation>正在載入摘要資料</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>Information</source>
        <translation>消息</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="204"/>
        <source>Pulse Rate</source>
        <translation>脈搏</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2813"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2837"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2839"/>
        <source>Rise Time</source>
        <translation>上升時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>Cheyne Stokes Respiration</source>
        <translation>潮式呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="133"/>
        <source>SmartStart</source>
        <translation>自啟動</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>同行顯示最近一個小時的AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="284"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>圖形顯示最近一個小時的RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="162"/>
        <source>Temperature Enable</source>
        <translation>温度测量啟用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="675"/>
        <source>Seconds</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="300"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 天): </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>Desktop OpenGL</source>
        <translation>桌面OpenGL</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="353"/>
        <source>Snapshot %1</source>
        <translation>快照 %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="844"/>
        <source>Mask Time</source>
        <translation>面罩使用時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>体感(0-無效，10=喜欢到停不下来)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time in REM Sleep</source>
        <translation>眼動睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <source>Channel</source>
        <translation>通道</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Time In Deep Sleep</source>
        <translation>深層睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Time in Deep Sleep</source>
        <translation>深層睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>Obstructive</source>
        <translation>阻塞性</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Pressure Max</source>
        <translation>最大壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Pressure Min</source>
        <translation>最小壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2908"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>PAP主管内径</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Max Leaks</source>
        <translation>最大漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Time to Sleep</source>
        <translation>睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation>呼吸努力指數與觉醒有关：呼吸限制會導致觉醒或者睡眠障碍.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="139"/>
        <source>Humid. Status</source>
        <translation>湿化器状态</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1120"/>
        <source>(Sess: %1)</source>
        <translation>（療程:%1）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="181"/>
        <source>Climate Control</source>
        <translation>恒温控制</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Perf. Index %</source>
        <translation>灌注指數 %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="204"/>
        <source>Standard</source>
        <translation>標準</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="57"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>如果您过往的資料已经丢失，請手動將所有的 Journal_XXXXXXX 資料夾内的檔案拷贝到此.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <source>&amp;Cancel</source>
        <translation>&amp;取消</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1601"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1610"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>最小呼氣壓力%1 最大吸氣壓力%2 壓力 %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <source>Default</source>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>Breaths/min</source>
        <translation>呼吸次数/分鐘</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>Degrees</source>
        <translation>度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <source>&amp;Destroy</source>
        <translation>&amp;删除</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>User Flag #1</source>
        <translation>使用者標記#1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>User Flag #2</source>
        <translation>使用者標記#2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>User Flag #3</source>
        <translation>使用者標記#3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="598"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>OSCAR现在將啟動匯入小幫手，以便您可以重新安装%1資料。</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="285"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>呼吸作用指數=%1 打鼾指數=%2 氣流受限指數=%3 周期性呼吸/潮湿呼吸=%4%%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="281"/>
        <source>Median rate of detected mask leakage</source>
        <translation>面罩漏氣率的中間值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Bookmark Notes</source>
        <translation>標記備註</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Bookmark Start</source>
        <translation>標記开始</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>PAP Device Mode</source>
        <translation>正压通氣模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="229"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="232"/>
        <source>Mask Pressure</source>
        <translation>面罩壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation>振動打鼾可被System One偵測到</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="367"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>尚未匯入血氧测定資料。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="47"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>請谨慎在OSCAR個人資料夾中操作:-P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=醒 2=眼動睡眠 3=淺層睡眠 4=深層睡眠</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="263"/>
        <source>Respiratory Event</source>
        <translation>呼吸事件</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="121"/>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Pressure Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Pressure Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>IPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>IPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>EPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>EPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>潮式呼吸的不正常時期</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="165"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="167"/>
        <source>Apnea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="167"/>
        <source>An apnea reportred by your CPAP machine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>未導致壓力上升的呼吸事件.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="356"/>
        <source>Debugging channel #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="356"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="357"/>
        <source>For internal use only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="356"/>
        <source>Test #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="357"/>
        <source>Debugging channel #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="357"/>
        <source>Test #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="169"/>
        <source>Antibacterial Filter</source>
        <translation>抗菌過濾棉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="37"/>
        <source>Windows User</source>
        <translation>Windows使用者</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="694"/>
        <source>Cataloguing EDF Files...</source>
        <translation>正在给EDF檔案編輯目录...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <source>Question</source>
        <translation>问题</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time spent in light sleep</source>
        <translation>淺層睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1130"/>
        <source>Waketime: %1</source>
        <translation>觉醒時間:%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time In REM Sleep</source>
        <translation>眼動睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>更高的吸氣壓力</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="284"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation>請拔出内存卡，插入PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="573"/>
        <source>The machine data folder needs to be removed manually.</source>
        <translation>數據資料夾需要手動移除.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>Summary Only</source>
        <translation>仅有概要資訊</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Bookmark End</source>
        <translation>標記結束</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2794"/>
        <source>Bi-Level</source>
        <translation>双水平</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="845"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="115"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="192"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="125"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="742"/>
        <source>Finishing Up...</source>
        <translation>整理中...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>Events/hr</source>
        <translation>事件/小時</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2854"/>
        <source>PRS1 humidifier connected?</source>
        <translation>PRS1 加湿器是否连接?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>CPAP Session contains summary data only</source>
        <translation>仅含有概要資料</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2748"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="368"/>
        <location filename="../oscar/mainwindow.cpp" line="734"/>
        <location filename="../oscar/mainwindow.cpp" line="2333"/>
        <source>Finishing up...</source>
        <translation>整理中...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="377"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="380"/>
        <source>Duration</source>
        <translation>時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="852"/>
        <source>Scanning Files...</source>
        <translation>扫描檔案...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="679"/>
        <source>
Hours: %1</source>
        <translation>
小時：%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2804"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2806"/>
        <source>Flex Mode</source>
        <translation>Flex模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="814"/>
        <source>Sessions</source>
        <translation>療程</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3031"/>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation>機器無法檢測流量的療程期間。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2944"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2946"/>
        <source>Auto Off</source>
        <translation>自動關閉</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1621"/>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation>呼氣壓力 %1 吸氣壓力 %2 %3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="724"/>
        <source>Overview</source>
        <translation>總覽</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="525"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>您選取的資料夾不是空的，也不包含有效的OSCAR資料。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="158"/>
        <source>Temperature</source>
        <translation>温度</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="427"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>全天氣流波形</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="512"/>
        <location filename="../oscar/main.cpp" line="564"/>
        <location filename="../oscar/main.cpp" line="579"/>
        <source>Exiting</source>
        <translation>正在退出</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time in Light Sleep</source>
        <translation>淺層睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time In Light Sleep</source>
        <translation>淺層睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Fixed Bi-Level</source>
        <translation>固定双水平</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="264"/>
        <source>Machine Information</source>
        <translation>機器資訊</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Pressure Support Maximum</source>
        <translation>壓力支持最大值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="266"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>圖形顯示氣流限制的嚴重程度</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="191"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>:%1 小時, %2 分鐘, %3 秒
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>全自動双水平(壓力可变)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2953"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2955"/>
        <source>Mask Alert</source>
        <translation>面罩报警</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="284"/>
        <source>OSCAR Reminder</source>
        <translation>OSCAR提醒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="44"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>OSCAR找到先前的日誌資料夾，已被重命名:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>A partially obstructed airway</source>
        <translation>氣道部分阻塞</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>Pressure Support Minimum</source>
        <translation>壓力支持最小值</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="182"/>
        <source>Large Leak</source>
        <translation>大量漏氣</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="297"/>
        <source>Time started according to str.edf</source>
        <translation>依據 str.edf 計時</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="843"/>
        <source>Wake-up</source>
        <translation>醒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Min Pressure</source>
        <translation>最小壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>Total Leak Rate</source>
        <translation>總漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Max Pressure</source>
        <translation>最大壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="810"/>
        <source>MaskPressure</source>
        <translation>面罩壓力</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1129"/>
        <source>Duration %1:%2:%3</source>
        <translation>時長 %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1130"/>
        <source>AHI %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="778"/>
        <source>Upper Threshold</source>
        <translation>增加</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="46"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>OSCAR不會變更此資料夾，將會創建一個新資料夾。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="808"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>Total Leaks</source>
        <translation>總漏氣量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Minute Ventilation</source>
        <translation>分鐘通氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Rate of detected mask leakage</source>
        <translation>面罩漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="226"/>
        <source>Breathing flow rate waveform</source>
        <translation>呼吸流量波形</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>Lower Expiratory Pressure</source>
        <translation>更低的呼氣壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>SD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="232"/>
        <source>Mask Pressure (High frequency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="263"/>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="294"/>
        <source>Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="294"/>
        <source>Movement detector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time spent in REM Sleep</source>
        <translation>眼動睡眠時長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1588"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>最小 %1 最大%2(%3)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="469"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>如果正在使用云儲存，請确保OSCAR已關閉，並且在繼續操作之前已在另一台計算机上完成同步。</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="279"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>中止指數=%1 低通氣指數=%2 中枢性中止指數=%3 </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="260"/>
        <source>Time taken to breathe in</source>
        <translation>吸氣時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Maximum Therapy Pressure</source>
        <translation>最大治療壓力</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="526"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>确认選取這個資料夾吗?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="610"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>使用檔案管理器复制個人檔案目录，然后重新啟動oscar並完成升级过程。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="163"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation>OSCAR只能跟踪该機器的使用時間和基本的設定。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="302"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 天): </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="429"/>
        <source>Current Selection</source>
        <translation>当前選取</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="209"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>血氧飽和百分比</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="548"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR保留了设备資料卡的備份&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="260"/>
        <source>Inspiratory Time</source>
        <translation>吸氣時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Respiratory Rate</source>
        <translation>呼吸频率</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2246"/>
        <source>Hide All Events</source>
        <translation>隐藏所有事件</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="99"/>
        <source>Printing %1 Report</source>
        <translation>正在列印%1報告</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="257"/>
        <source>Expiratory Time</source>
        <translation>呼氣時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="188"/>
        <source>Expiratory Puff</source>
        <translation>嘴部呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Maximum Leak</source>
        <translation>最大漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="254"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>呼氣和吸氣時間的比率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>APAP (Variable)</source>
        <translation>APAP（自動）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Minimum Therapy Pressure</source>
        <translation>最小治療壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>心率突变</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Body Mass Index</source>
        <translation>体重指數</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="725"/>
        <source>Oximetry</source>
        <translation>血氧测定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="727"/>
        <source>Oximeter</source>
        <translation>血氧儀</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="700"/>
        <source>No Data Available</source>
        <translation>無可用資料</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>The maximum rate of mask leakage</source>
        <translation>面罩的最大漏氣率</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="843"/>
        <source>Backing Up Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="150"/>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="92"/>
        <source>Untested Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="916"/>
        <source>model %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="922"/>
        <source>DreamStation 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="925"/>
        <source>unknown model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="175"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="440"/>
        <source>Machine Untested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2790"/>
        <source>CPAP-Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2792"/>
        <source>AutoCPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2793"/>
        <source>Auto-Trial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2795"/>
        <source>AutoBiLevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2797"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2798"/>
        <source>S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2800"/>
        <source>S/T - AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2801"/>
        <source>PC - AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2809"/>
        <source>C-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2810"/>
        <source>C-Flex+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2811"/>
        <source>A-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2812"/>
        <source>P-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2814"/>
        <source>Bi-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2816"/>
        <source>Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2820"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2822"/>
        <source>Flex Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2828"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2830"/>
        <source>Flex Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2829"/>
        <source>Whether Flex settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2838"/>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2844"/>
        <source>Rise Time Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2845"/>
        <source>Whether Rise Time settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2846"/>
        <source>Rise Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2853"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="139"/>
        <source>Humidifier Status</source>
        <translation>加湿器状态</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2862"/>
        <source>Humidification Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2863"/>
        <source>PRS1 Humidification Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2864"/>
        <source>Humid. Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2866"/>
        <source>Fixed (Classic)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2867"/>
        <source>Adaptive (System One)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2868"/>
        <source>Heated Tube</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2869"/>
        <source>Passover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2874"/>
        <source>Tube Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2875"/>
        <source>PRS1 Heated Tube Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2876"/>
        <source>Tube Temp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2883"/>
        <source>PRS1 Humidifier Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2890"/>
        <source>Target Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2891"/>
        <source>PRS1 Humidifier Target Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2892"/>
        <source>Hum. Tgt Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2899"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2900"/>
        <source>Mask Resistance Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2901"/>
        <source>Mask Resist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2909"/>
        <source>Hose Diam.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2911"/>
        <source>22mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2912"/>
        <source>15mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2913"/>
        <source>12mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2917"/>
        <source>Tubing Type Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2918"/>
        <source>Whether tubing type settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2919"/>
        <source>Tube Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2926"/>
        <source>Mask Resistance Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2927"/>
        <source>Whether mask resistance settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2928"/>
        <source>Mask Res. Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2963"/>
        <source>Whether or not machine shows AHI via built-in display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2971"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2973"/>
        <source>Ramp Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2972"/>
        <source>Type of ramp curve to use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2975"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2976"/>
        <source>SmartRamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2977"/>
        <source>Ramp+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2981"/>
        <source>Backup Breath Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2982"/>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2983"/>
        <source>Breath Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2987"/>
        <source>Fixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2991"/>
        <source>Fixed Backup Breath BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2992"/>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2993"/>
        <source>Breath BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2998"/>
        <source>Timed Inspiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2999"/>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3000"/>
        <source>Timed Insp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3005"/>
        <source>Auto-Trial Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3006"/>
        <source>The number of days in the Auto-CPAP trial period, after which the machine will revert to CPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3007"/>
        <source>Auto-Trial Dur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3012"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3014"/>
        <source>EZ-Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3013"/>
        <source>Whether or not EZ-Start is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3021"/>
        <source>Variable Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3022"/>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3032"/>
        <source>BND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3038"/>
        <source>Machine Initiated Breath</source>
        <translation>呼吸触发機器开启</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3039"/>
        <source>TB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3045"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3047"/>
        <source>Peak Flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3046"/>
        <source>Peak flow during a 2-minute interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="561"/>
        <source>Machine Database Changes</source>
        <translation>資料库變更</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2786"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2788"/>
        <source>SmartFlex Mode</source>
        <translation>SmartFlex模式</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Journal Notes</source>
        <translation>日誌備註</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="385"/>
        <source> (%2 min, %3 sec)</source>
        <translation> (%2 分, %3 秒)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="468"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>一次只能处理单個OSCAR個人檔案的一個实例。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Expiratory Pressure</source>
        <translation>呼氣壓力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2962"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2964"/>
        <source>Show AHI</source>
        <translation>顯示AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>Tgt. Min. Vent</source>
        <translation>目標 分鐘 通氣</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="582"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>由%1備份重建中</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1254"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>確定將所有的波形通道颜色重新設定為預設值吗?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2780"/>
        <source>Pressure Pulse</source>
        <translation>壓力脉冲</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="826"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>療程: %1 / %2 / %3 长度: %4 / %5 / %6 最长: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="748"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2855"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2882"/>
        <source>Humidifier</source>
        <translation>湿度</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="155"/>
        <source>Relief: %1</source>
        <translation>壓力释放： %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="839"/>
        <source>Patient ID</source>
        <translation>患者编号</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="123"/>
        <source>Patient???</source>
        <translation>病患???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>氣道阻塞状态下的呼吸中止</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>震動式打鼾 (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="15"/>
        <source>Please Wait...</source>
        <translation>請稍候...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="268"/>
        <source>CMS50D+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="268"/>
        <source>CMS50E/F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="40"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="42"/>
        <source>Contec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="40"/>
        <source>CMS50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="42"/>
        <source>CMS50F3.7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="42"/>
        <source>CMS50F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/dreem_loader.h" line="37"/>
        <source>Dreem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>Fisher &amp; Paykel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <source>ICON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="40"/>
        <source>ChoiceMMed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="40"/>
        <source>MD300</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="67"/>
        <source>Respironics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="67"/>
        <source>M-Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>Philips Respironics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>System One</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1020"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1021"/>
        <source>SensAwake level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1028"/>
        <source>Expiratory Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1034"/>
        <source>Expiratory Relief Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1046"/>
        <source>Humidity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>SleepStyle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="93"/>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="94"/>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="117"/>
        <source>Weinmann</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="117"/>
        <source>SOMNOsoft2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="152"/>
        <source>New versions file improperly formed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="175"/>
        <source>A more recent version of OSCAR is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>test version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="171"/>
        <source>You are running the latest %1 of OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="172"/>
        <location filename="../oscar/checkupdates.cpp" line="176"/>
        <source>You are running OSCAR %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="178"/>
        <source>OSCAR %1 is available &lt;a href=&apos;%2&apos;&gt;here&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="181"/>
        <source>Information about more recent test version %1 is available at &lt;a href=&apos;%2&apos;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="209"/>
        <source>Check for OSCAR Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="276"/>
        <source>Unable to check for updates. Please try again later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="349"/>
        <source>%1 Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="350"/>
        <source>%1 of %2 Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="577"/>
        <source>Loading summaries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="81"/>
        <source>This page in other languages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2600"/>
        <source>%1 Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2601"/>
        <source>%1 of %2 Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2620"/>
        <source>%1 Event Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2621"/>
        <source>%1 of %2 Event Types</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <location filename="../oscar/reports.ui" line="27"/>
        <source>about:blank</source>
        <translation>关於:空白</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="247"/>
        <source>%1h %2m</source>
        <translation>%1% %2m</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="290"/>
        <source>No Sessions Present</source>
        <translation>没有療程</translation>
    </message>
</context>
<context>
    <name>SleepStyleLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>無法在此個人檔案中匯入此设备的记录。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="213"/>
        <source>Import Error</source>
        <translation>匯入出错</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>本日的資料已覆蓋已儲存的内容.</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="984"/>
        <source>Days</source>
        <translation>天数</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1488"/>
        <source>Worst Flow Limtation</source>
        <translation>最差的流量限值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1521"/>
        <source>Worst Large Leaks</source>
        <translation>最大漏氣量</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="586"/>
        <source>Oximeter Statistics</source>
        <translation>血氧儀統計值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1529"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>日期: %1 Leak: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <location filename="../oscar/statistics.cpp" line="1397"/>
        <source>CPAP Usage</source>
        <translation>CPAP使用情况</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Blood Oxygen Saturation</source>
        <translation>血氧飽和度</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1601"/>
        <source>No PB on record</source>
        <translation>無周期性呼吸資料</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="613"/>
        <source>% of time in %1</source>
        <translation>% 在 %1 時間中</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1166"/>
        <source>Last 30 Days</source>
        <translation>最近三十天</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1609"/>
        <source>Want more information?</source>
        <translation>更多資訊?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1398"/>
        <source>Days Used: %1</source>
        <translation>天数:%1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="612"/>
        <source>%1 Index</source>
        <translation>%1 指數</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1634"/>
        <location filename="../oscar/statistics.cpp" line="1646"/>
        <source>Date: %1 - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1637"/>
        <location filename="../oscar/statistics.cpp" line="1649"/>
        <source>AHI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1638"/>
        <location filename="../oscar/statistics.cpp" line="1650"/>
        <source>Total Hours: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1643"/>
        <source>Worst RX Setting</source>
        <translation>最差治療方案设定</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1631"/>
        <source>Best RX Setting</source>
        <translation>最佳治療方案设定</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1234"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 天在 %2 中的資料在 %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1566"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>日期: %1 CSR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="614"/>
        <source>% of time above %1 threshold</source>
        <translation>% 的時間高於 %1 閥值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="543"/>
        <source>Therapy Efficacy</source>
        <translation>疗效</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="615"/>
        <source>% of time below %1 threshold</source>
        <translation>% 的時間低於 %1 閥值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="611"/>
        <source>Max %1</source>
        <translation>最大 %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="606"/>
        <source>%1 Median</source>
        <translation>%1 中值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="610"/>
        <source>Min %1</source>
        <translation>最小 %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="734"/>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="737"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="976"/>
        <source>Changes to Machine Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1062"/>
        <source>No data found?!?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1064"/>
        <source>Oscar has no data to report :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1164"/>
        <source>Most Recent</source>
        <translation>最近</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1610"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1611"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>請在属性选单中选中預调取彙總資訊選項.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="990"/>
        <source>Pressure Settings</source>
        <translation>壓力設定</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="642"/>
        <source>Phone: %1</source>
        <translation>电话号码:%1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1588"/>
        <source>Worst PB</source>
        <translation>最差周期性呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="561"/>
        <source>Pressure Statistics</source>
        <translation>壓力統計值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="637"/>
        <source>Name: %1, %2</source>
        <translation>名字: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1167"/>
        <source>Last 6 Months</source>
        <translation>最近六個月</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="645"/>
        <source>Email: %1</source>
        <translation>电子邮箱: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="607"/>
        <location filename="../oscar/statistics.cpp" line="608"/>
        <source>Average %1</source>
        <translation>平均 %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1231"/>
        <source>No %1 data available.</source>
        <translation>%1 資料可用.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="924"/>
        <source>Last Use</source>
        <translation>最近一次</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="988"/>
        <source>Pressure Relief</source>
        <translation>壓力释放</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="639"/>
        <source>DOB: %1</source>
        <translation>生日:%1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>Pulse Rate</source>
        <translation>脈搏</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="923"/>
        <source>First Use</source>
        <translation>首次</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1558"/>
        <source>Worst CSR</source>
        <translation>最差的潮式呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1441"/>
        <source>Worst AHI</source>
        <translation>最高的AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1165"/>
        <source>Last Week</source>
        <translation>上週</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1168"/>
        <source>Last Year</source>
        <translation>去年</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1478"/>
        <source>Best Flow Limitation</source>
        <translation>最好的流量限值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="648"/>
        <source>Address:</source>
        <translation>地址:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1217"/>
        <source>Details</source>
        <translation>詳細資料</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1500"/>
        <source>No Flow Limitation on record</source>
        <translation>無流量限值记录</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1240"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 天的在 %2中的資料,在%3 和 %4 之間</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1535"/>
        <source>No Large Leaks on record</source>
        <translation>無大量漏氣记录</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1596"/>
        <source>Date: %1 PB: %2%</source>
        <translation>日期: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1431"/>
        <source>Best AHI</source>
        <translation>最低AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1172"/>
        <source>Last Session</source>
        <translation>上一個療程</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1435"/>
        <location filename="../oscar/statistics.cpp" line="1447"/>
        <source>Date: %1 AHI: %2</source>
        <translation>日期: %1 AHI: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="917"/>
        <source>Machine Information</source>
        <translation>機器資訊</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="536"/>
        <source>CPAP Statistics</source>
        <translation>CPAP統計值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1400"/>
        <source>Compliance: %1%</source>
        <translation>依從: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1482"/>
        <location filename="../oscar/statistics.cpp" line="1495"/>
        <source>Date: %1 FL: %2</source>
        <translation>日期: %1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1424"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>AHI大於5的天数: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1399"/>
        <source>Low Use Days: %1</source>
        <translation>低使用天数:%1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="556"/>
        <source>Leak Statistics</source>
        <translation>漏氣統計值</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1571"/>
        <source>No CSR on record</source>
        <translation>無潮式呼吸记录</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="540"/>
        <source>Average Hours per Night</source>
        <translation>平均每晚的小時数</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>over</source>
        <translation>高於</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="209"/>
        <source>under</source>
        <translation>低於</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="257"/>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation>您的呼吸器使用固定%1 %2加壓空氣</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="317"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>平均漏氣為 %1 %2，即 %3 您的 %5 天 %4 平均值。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="580"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap machine.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;span style=&quot; font-weight:600;&quot;&gt;注意：&lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;請確認 PAP 專用記憶卡的覆寫保護已開啟&lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;特別是在插入其它電腦裝置之前&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;有些作業系統會在偵測到連接媒體時，自動寫入索引檔案且無預設提示通知，此類型系統寫入動作可能導致呼吸器將無法辨識讀取記憶卡&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="127"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>歡迎使用開放資源 CPAP 解析彙整程式</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="285"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>呼氣壓力固定於 %1 %2。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="323"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>尚未匯入呼吸器資料。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="261"/>
        <source>Daily View</source>
        <translation>每日概況</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="223"/>
        <source>Oximetry Wizard</source>
        <translation>血氧測定儀小幫手</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation>昨晚</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="142"/>
        <source>What would you like to do?</source>
        <translation>您打算從何處著手？</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="184"/>
        <source>was %1 (on %2)</source>
        <translation>為 %1 (於 %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation>有些至關重要的偏好選項會影響資料匯入.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="195"/>
        <source>Your machine was on for %1.</source>
        <translation>呼吸器使用時間為 %1。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="226"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>您的 AHI 相等於%1, 即 %2 您的 %3 天 %4 的平均值。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;您有戴著呼吸罩使用機器的計時只有 %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation>首次記錄導入需耗時數分鐘。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="212"/>
        <source>equal to</source>
        <translation>等於</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>開始的第一步，先檢查 檔案 --&gt; 偏好選項，</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>%2 days ago</source>
        <translation>%2 天前</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>1 day ago</source>
        <translation>1 天前</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="337"/>
        <source>Statistics</source>
        <translation>統計數據</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="270"/>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation>您的呼吸機使用固定 %1 %2 %3 加壓空氣。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="185"/>
        <source>CPAP Importer</source>
        <translation>CPAP 導入器</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="262"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>壓力低於 %1 %2，持續時間%3%.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="299"/>
        <source>Overview</source>
        <translation>綜合概況</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="277"/>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation>呼吸器使用時數低於 %1-%2 %3 ，持續時間%4% 。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>reasonably close to</source>
        <translation>合理地近似</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="296"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>呼氣壓力低於 %1 %2，持續時間 %3%。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="288"/>
        <location filename="../oscar/welcome.cpp" line="297"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>吸氣壓力低於 %1 %2，持續時間 %3%。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation>請注意，在偵測到 ResMed 設備時某些偏好選項會直接套用</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="192"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 小時，%2分 %3 秒</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation>您上次使用 %1...</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="796"/>
        <source>%1 days</source>
        <translation>%1天</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2088"/>
        <source>Clone %1 Graph</source>
        <translation>复制 %1 圖表</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="414"/>
        <source>Oximeter Overlays</source>
        <translation>血氧儀 覆蓋</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="397"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="399"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="406"/>
        <source>Plots</source>
        <translation>區塊</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="402"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>重新設定所有图标到统一的高度以及預設顺序.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1786"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1839"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2084"/>
        <source>Remove Clone</source>
        <translation>移除複製</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="417"/>
        <source>Dotted Lines</source>
        <translation>虛線</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="411"/>
        <source>CPAP Overlays</source>
        <translation>CPAP 覆蓋</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="405"/>
        <source>Y-Axis</source>
        <translation>Y轴</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="401"/>
        <source>Reset Graph Layout</source>
        <translation>重新設定圖表配置</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="395"/>
        <source>100% zoom level</source>
        <translation>100% 缩放级别</translation>
    </message>
</context>
</TS>
